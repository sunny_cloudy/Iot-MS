package cn.ljobin.bibi.utils;

import org.apache.http.client.HttpClient;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

/**
 * @program: guns
 * @description: httpclient客户端
 * @author: Mr.Liu
 * @create: 2019-10-10 09:12
 **/
public class HttpClientUtil {
    //得到HttpClient
    public HttpClient getHttpClient(){
        HttpParams mHttpParams=new BasicHttpParams();
        //设置网络链接超时
        //即:Set the timeout in milliseconds until a connection is established.
        HttpConnectionParams.setConnectionTimeout(mHttpParams, 20*1000);
        //设置socket响应超时
        //即:in milliseconds which is the timeout for waiting for data.
        HttpConnectionParams.setSoTimeout(mHttpParams, 20*1000);
        //设置socket缓存大小
        HttpConnectionParams.setSocketBufferSize(mHttpParams, 8*1024);
        //设置是否可以重定向
        HttpClientParams.setRedirecting(mHttpParams, true);

        HttpClient httpClient=new DefaultHttpClient(mHttpParams);
        return httpClient;
    }
}
