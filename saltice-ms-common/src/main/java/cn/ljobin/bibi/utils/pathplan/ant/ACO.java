package cn.ljobin.bibi.utils.pathplan.ant;

import cn.ljobin.bibi.utils.pathplan.MapNode;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @program: guns
 * @description: 蚁群优化算法，用来求解TSP问题
 * @author: Mr.Liu
 * @create: 2019-09-29 20:47
 **/


public class ACO {

    /**定义蚂蚁群**/
    ant []ants;
    /**蚂蚁的数量**/
    int antcount;
    /**表示城市间距离**/
    double [][]distance;
    /**信息素矩阵**/
    double [][]tao;
    /**城市数量**/
    int citycount;
    /**求解的最佳路径**/
    int[]besttour;
    /**求的最优解的长度**/
    double bestlength;
    //filename tsp数据文件
    //antnum 系统用到蚂蚁的数量
    /**
     * 文件内容格式
     10
     A 2 4
     B 2 3
     C 1 5
     **/
    public void init(String filename,int antnum) throws FileNotFoundException, IOException{
        antcount=antnum;
        ants=new ant[antcount];
        //读取数据tsp里的数据包括第I个城市与城市的X,Y坐标
        int[] x;
        int[] y;
        String strbuff;
        BufferedReader tspdata = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
        //读取第一行，城市总数（按文件格式读取）
        strbuff = tspdata.readLine();
        citycount = Integer.parseInt(strbuff);
        distance = new double[citycount][citycount];
        x = new int[citycount];
        y = new int[citycount];
        for (int citys = 0; citys < citycount; citys++) {
            strbuff = tspdata.readLine();
            String[] strcol = strbuff.split(" ");
            //读取每排数据的第2二个数字即横坐标
            x[citys] = Integer.parseInt(strcol[1]);
            y[citys] = Integer.parseInt(strcol[2]);
        }
        //计算两个城市之间的距离矩阵，并更新距离矩阵
        for (int city1 = 0; city1 < citycount - 1; city1++) {
            distance[city1][city1] = 0;
            for (int city2 = city1 + 1; city2 < citycount; city2++) {
                distance[city1][city2] = (int) (Math.sqrt((x[city1] - x[city2]) * (x[city1] - x[city2])
                        + (y[city1] - y[city2]) * (y[city1] - y[city2])));
                //距离矩阵是对称矩阵
                distance[city2][city1] = distance[city1][city2];
            }
        }
        distance[citycount - 1][citycount - 1] = 0;
        //初始化信息素矩阵
        tao=new double[citycount][citycount];
        for(int i=0;i<citycount;i++)
        {
            for(int j=0;j<citycount;j++){
                tao[i][j]=0.1;
            }
        }
        bestlength=Integer.MAX_VALUE;
        besttour=new int[citycount+1];
        //随机放置蚂蚁
        for(int i=0;i<antcount;i++){
            ants[i]=new ant();
            ants[i].RandomSelectCity(citycount,-1);
        }
    }

    /**
     *  数据data是Map<String,Object> 其中 key = cityNum 为城市数目  ，  key = data 为数据List<CityNode>
     * @param data
     * @param antnum 蚂蚁数目
     * @param whoCity 表示以哪个城市为出发点以及回溯点 -1代表随机
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void init(Map<String,Object> data, int antnum,int whoCity) throws FileNotFoundException, IOException{
        antcount=antnum;
        ants=new ant[antcount];
        //读取数据tsp里的数据包括第I个城市与城市的X,Y坐标
        double[] x;
        double[] y;

        //读取第一行，城市总数
        citycount = (Integer)data.get("cityNum");
        distance = new double[citycount][citycount];
        x = new double[citycount];
        y = new double[citycount];
        List<MapNode> city = (ArrayList<MapNode>)data.get("data");
        for (int citys = 0; citys < citycount; citys++) {
            //读取每排数据的第2二个数字即横坐标
            x[citys] = city.get(citys).getX();
            y[citys] = city.get(citys).getY();
        }
        //计算两个城市之间的距离矩阵，并更新距离矩阵
        for (int city1 = 0; city1 < citycount - 1; city1++) {
            distance[city1][city1] = 0.0;
            for (int city2 = city1 + 1; city2 < citycount; city2++) {
                distance[city1][city2] =  (Math.sqrt((x[city1] - x[city2]) * (x[city1] - x[city2])
                        + (y[city1] - y[city2]) * (y[city1] - y[city2])));
                //距离矩阵是对称矩阵
                distance[city2][city1] = distance[city1][city2];
            }
        }
        distance[citycount - 1][citycount - 1] = 0.0;
        //初始化信息素矩阵
        tao=new double[citycount][citycount];
        for(int i=0;i<citycount;i++)
        {
            for(int j=0;j<citycount;j++){
                tao[i][j]=0.1;
            }
        }
        bestlength=Integer.MAX_VALUE;
        besttour=new int[citycount+1];
        //随机放置蚂蚁
        for(int i=0;i<antcount;i++){
            ants[i]=new ant();
            ants[i].RandomSelectCity(citycount,whoCity);
        }
    }
    //maxgen ACO的最多循环次数
    public void run(int maxgen,int whoCity){
        for(int runtimes=0;runtimes<maxgen;runtimes++){
            //每次迭代，所有蚂蚁都要跟新一遍，走一遍
            //System.out.print("no>>>"+runtimes);
            //每一只蚂蚁移动的过程
            for(int i=0;i<antcount;i++){
                for(int j=1;j<citycount;j++){
                    //每只蚂蚁的城市规划
                    ants[i].SelectNextCity(j,tao,distance);
                }
                //计算蚂蚁获得的路径长度
                ants[i].CalTourLength(distance);
                if(ants[i].tourlength<bestlength){
                    //保留最优路径
                    bestlength=ants[i].tourlength;
                    //runtimes仅代表最大循环次数，但是只有当，有新的最优路径的时候才会显示下列语句。
                    //如果后续没有更优解（收敛），则最后直接输出。
                    System.out.println("第"+runtimes+"代(次迭代)，发现新的最优路径长度："+bestlength);
                    for(int j=0;j<citycount+1;j++)
                        //更新路径
                        besttour[j]=ants[i].tour[j];
                }
            }
            //更新信息素矩阵
            UpdateTao();
            //重新随机设置蚂蚁
            for(int i=0;i<antcount;i++){
                ants[i].RandomSelectCity(citycount,whoCity);
            }
        }
    }
    /**
     * 更新信息素矩阵
     */
    private void UpdateTao(){
        double rou=0.5;
        //信息素挥发
        for(int i=0;i<citycount;i++)
            for(int j=0;j<citycount;j++)
                tao[i][j]=tao[i][j]*(1-rou);
        //信息素更新
        for(int i=0;i<antcount;i++){
            for(int j=0;j<citycount;j++){
                tao[ants[i].tour[j]][ants[i].tour[j+1]]+=1.0/ants[i].tourlength;
            }
        }
    }
    /* 输出程序运行结果
     */
    public int[] ReportResult(){
        System.out.println("最优路径长度是"+bestlength);
        System.out.println("蚁群算法最优路径输出：");
        for(int j=0;j<citycount+1;j++){
            //输出最优路径
            System.out.print( besttour[j]+">>");
        }
            return besttour;
    }
}
