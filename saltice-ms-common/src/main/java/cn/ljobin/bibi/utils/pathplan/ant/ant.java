package cn.ljobin.bibi.utils.pathplan.ant;
import java.util.Random;

/**
 * @program: guns
 * @description: 蚂蚁类
 * @author: Mr.Liu
 * @create: 2019-09-29 20:48
 **/

public class ant {
    /**
     * 蚂蚁获得的路径
     */
    //参观城市顺序
    public int[]tour;
    //unvisitedcity 取值是0或1，1表示没有访问过，0表示访问过
    int[] unvisitedcity;
    /**
     * 蚂蚁获得的路径长度
     */
    //某蚂蚁所走路程总长度。
    public double tourlength;
    int citys;//城市个数
    /**
     * 随机分配蚂蚁到某个城市中 ，如果
     * 同时完成蚂蚁包含字段的初始化工作
     * @param citycount 总的城市数量
     * @param city 蚂蚁分配到那个城市 -1代表随机
     */
    public void RandomSelectCity(int citycount,int city){
        citys=citycount;
        unvisitedcity=new int[citycount];
        tour=new int[citycount+1];
        tourlength=0;
        for(int i=0;i<citycount;i++){
            tour[i]=-1;
            unvisitedcity[i]=1;
        }//初始化各个变量

        //获取当前时间
        long r1 = System.currentTimeMillis();
        Random rnd=new Random(r1);
        int firstcity = 0;
        if(city==-1){
            //随机指定第一个城市
            firstcity=rnd.nextInt(citycount);
        }else {
            /**默认从第一个城市开始**/
            firstcity= city;
        }
        //0表示访问过
        unvisitedcity[firstcity]=0;
        //起始城市
        tour[0]=firstcity;
    }
    /**
     * 选择下一个城市
     * @param index 需要选择第index个城市了
     * @param tao   全局的信息素信息
     * @param distance  全局的距离矩阵信息
     */
    public void SelectNextCity(int index,double[][]tao,double[][]distance){
        double []p;
        //下一步要走的城市的选中概率
        p=new double[citys];
        //产生一个[0,1]和[10,11]的double数值 用来求选定概率
        int alpha= 2;//Math.floor(random.nextDouble());
        //该数在10周围比较好，但是值越大速度越慢
        int beta =2;//10+Math.floor(random.nextDouble());
        double sum=0;
        //蚂蚁所处当前城市
        int currentcity=tour[index-1];
        //计算公式中的分母部分（为下一步计算选中概率使用）
        for(int i=0;i<citys;i++){
            //没走过
            if(unvisitedcity[i]==1)
                sum+=(Math.pow(tao[currentcity][i], alpha)*
                        Math.pow(1.0/distance[currentcity][i], beta));
        }
        //计算每个城市被选中的概率
        for(int i=0;i<citys;i++){
            if(unvisitedcity[i]==0)
                //城市走过了，选中概率就是0
                p[i]=0.0;
            else{
                //没走过，下一步要走这个城市的概率是？
                p[i]=(Math.pow(tao[currentcity][i], alpha)*
                        Math.pow(1.0/distance[currentcity][i], beta))/sum;
            }
        }
        double selectp=Math.random();
        //轮盘赌选择一个城市；
        double sumselect=0;
        int selectcity=1;
        //城市选择随机，直到n个概率加起来大于随机数，则选择该城市
        //每次都是顺序走。。。。。
        for(int i=0;i<citys;i++){
            sumselect+=p[i];
            if(sumselect>=selectp){
                selectcity=i;
                break;
            }
        }
        //这个城市没有走过
        if (selectcity==1){
            //System.out.println();
        }
        tour[index]=selectcity;
        unvisitedcity[selectcity]=0;
    }
    /**
     * 计算蚂蚁获得的路径的长度
     * @param distance  全局的距离矩阵信息
     */
    public void CalTourLength(double [][]distance){
        tourlength=0;
        //第一个城市等于最后一个要到达的城市
        tour[citys]=tour[0];
        for(int i=0;i<citys;i++){
            //从A经过每个城市仅一次，最后回到A的总长度。
            tourlength+=distance[tour[i]][tour[i+1]];
        }
    }
}
