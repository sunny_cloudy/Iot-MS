package cn.ljobin.bibi.utils.pathplan.ant;

/**
 * @program: guns
 * @description:
 * @author: Mr.Liu
 * @create: 2019-09-29 20:48
 **/

import cn.ljobin.bibi.utils.pathplan.AstarPathPlan;
import cn.ljobin.bibi.utils.pathplan.MapNode;

import java.io.IOException;
import java.util.*;

//蚁群算法求解旅行商问题，TSP数据来源
//http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/
//数据中包括城市总量，每个城市的横纵坐标
public class Main {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<MapNode> cityNodeList = new ArrayList<>();
        cityNodeList.add(new MapNode(2.0,4.0));
        cityNodeList.add(new MapNode(2.0,3.0));
        cityNodeList.add(new MapNode(1,5));
        cityNodeList.add(new MapNode(2,1));
        cityNodeList.add(new MapNode(4,5));
        cityNodeList.add(new MapNode(3,7));
        cityNodeList.add(new MapNode(2,2));
        cityNodeList.add(new MapNode(5,4));
        cityNodeList.add(new MapNode(7,6));
        cityNodeList.add(new MapNode(8,3));

//        cityNodeList.add(new MapNode(3,4));
//        cityNodeList.add(new MapNode(7,9));
//        cityNodeList.add(new MapNode(9,5));
//        cityNodeList.add(new MapNode(9,1));
//        cityNodeList.add(new MapNode(5,5));
//        cityNodeList.add(new MapNode(7,7));
//        cityNodeList.add(new MapNode(1,9));
//        cityNodeList.add(new MapNode(6,3));
//        cityNodeList.add(new MapNode(7,9));
//        cityNodeList.add(new MapNode(10,3));
        antGetPathAndA(cityNodeList);
    }
    public static int[] antGetPath(List<MapNode> cityNodeList){
        ACO aco;
        aco=new ACO();
        int[] path=null;
        try {
            //城市信息文件，蚂蚁数量
            Map<String,Object> cityMap = new HashMap<>();
            cityMap.put("data",cityNodeList);
            cityMap.put("cityNum",cityNodeList.size());
            //起始点，也就是蚂蚁放在的城市
            int whoCity = 0;
            aco.init(cityMap, 200,whoCity);
            //迭代次数 3000次差不多了
            aco.run(3000,whoCity);
            path = aco.ReportResult();
            return path;
        } catch (IOException ex) {
            ex.printStackTrace();
            return path;
        }
    }
    public static int[] antGetPathAndA(List<MapNode> cityNodeList){
        ACO aco;
        aco=new ACO();
        int[] path=null;
        try {
            //城市信息文件，蚂蚁数量
            Map<String,Object> cityMap = new HashMap<>();
            cityMap.put("data",cityNodeList);
            cityMap.put("cityNum",cityNodeList.size());
            //起始点，也就是蚂蚁放在的城市
            int whoCity = 5;
            aco.init(cityMap, 200,whoCity);
            //迭代次数 3000次差不多了
            aco.run(3000,whoCity);
            path = aco.ReportResult();
            AstarPathPlan a = new AstarPathPlan(20, 20, 1, 0, 0);
            System.out.println();
            //a.map.print();
            System.out.println();
            Deque<MapNode> result = new ArrayDeque<>();
            for (int i = 1; i < cityNodeList.size()+1 ; i++) {
                result.addAll(a.pathPlanning(cityNodeList.get(path[i - 1]), cityNodeList.get(path[i])));
            }
            a.printResult(result);
            return path;
        } catch (IOException ex) {
            ex.printStackTrace();
           return path;
        }
    }
}
