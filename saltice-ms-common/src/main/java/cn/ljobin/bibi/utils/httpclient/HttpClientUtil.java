package cn.ljobin.bibi.utils.httpclient;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * @program: guns
 * @description: httpclient 封装
 * @author: Mr.Liu
 * @create: 2019-10-15 10:18
 **/
public class HttpClientUtil {
    /**
     * 发送get请求
     * @param url 请求地址
     * @return JSONObject
     */
    public static JSONObject httpClientGet(String url){
        if("".equals(url)||url==null){
            return null;
        }
        JSONObject resultJsonObject=null;
        BufferedReader bufferedReader=null;
        // 获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        // 创建Get请求 47.103.1.210:8082/guns-1.0.0
        HttpGet httpGet = new HttpGet(url);
        // 响应模型
        CloseableHttpResponse response = null;

        try {
            // 将上面的配置信息 运用到这个Get请求里
            httpGet.setConfig(initRequestConfig());
            StringBuilder entityStringBuilder=new StringBuilder();
            // 由客户端执行(发送)Get请求
            response = httpClient.execute(httpGet);
            //得到httpResponse的状态响应码
            int statusCode= response.getStatusLine().getStatusCode();
            if (statusCode== HttpStatus.SC_OK) {
                //得到httpResponse的实体数据 //响应实体
                HttpEntity httpEntity=response.getEntity();
                if (httpEntity!=null) {
                    try {
                        bufferedReader = new BufferedReader
                                (new InputStreamReader(httpEntity.getContent(), StandardCharsets.UTF_8), 8 * 1024);
                        String line = null;
                        while ((line = bufferedReader.readLine()) != null) {
                            entityStringBuilder.append(line);
                        }
                        //利用从HttpEntity中得到的String生成JsonObject
                        resultJsonObject = JSONObject.parseObject(entityStringBuilder.toString());
                        return resultJsonObject;
                    }catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

            }
            return null;
        }catch (ParseException | IOException e) {
            e.printStackTrace();
            return null;
        }finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 配置信息
     * @return
     */
    public static RequestConfig initRequestConfig(){
        // 配置信息
        return RequestConfig.custom()
                // 设置连接超时时间(单位毫秒)
                .setConnectTimeout(5000)
                // 设置请求超时时间(单位毫秒)
                .setConnectionRequestTimeout(5000)
                // socket读写超时时间(单位毫秒)
                .setSocketTimeout(5000)
                // 设置是否允许重定向(默认为true)
                .setRedirectsEnabled(true)
                .build();
    }
}
