package cn.ljobin.bibi.netty.util;

/**
 * @program: guns
 * @description: 语音模块使用工具类
 * @author: Mr.Liu
 * @create: 2019-10-21 12:54
 **/
public class InputComVoiceUtil {
    /**
     * 指令正确的返回检测
     * @param msg
     * @return
     */
    public static boolean isOkAckCode(String msg){
        return isOkCode(msg);
    }

    /**
     * 语音合成完毕的返回检测
     * @param msg
     * @return
     */
    public static boolean isEndAckCode(String msg){
        return isEndCode(msg);
    }

    private static boolean isOkCode(String msg){
        return "41".equals(msg);
    }
    private static boolean isEndCode(String msg){
        return "4F".equals(msg)||"4f".equals(msg);
    }
}
