package cn.ljobin.bibi.netty.util;

import java.util.Arrays;
import java.util.List;

public class OutputCommandUtil {

    private static String packetData(List<String> data){

        if(data != null){

            StringBuilder sb= new StringBuilder();

            data.forEach(item ->{

                sb.append(item);

            });

            return sb.toString();

        }

        return "";
    }

    public static String packet(List<String> data){

        StringBuilder sb=  new StringBuilder();

        String dataHexString = packetData(data);

        String lengthHexString = ByteUtil.numToLengthHexString(dataHexString.length()/2,2);


        sb.append("01")
                .append("03")
                .append(lengthHexString)
                .append(dataHexString);

        String crc =CRC16Util.calcCrc16String(sb.toString());

        sb.append(crc);

        //Rx:009779-01 03 14 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 A3 67


        return sb.toString();
    }



    public static void main(String[] args) {
        String a="00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00";
        List<String> strings = Arrays.asList(a.split(" "));
        String packet = OutputCommandUtil.packet(strings);
        System.out.println(packet);

    }

}
