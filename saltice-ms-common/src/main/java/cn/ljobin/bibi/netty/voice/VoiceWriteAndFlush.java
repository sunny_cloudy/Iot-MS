package cn.ljobin.bibi.netty.voice;

import cn.ljobin.bibi.netty.util.ByteUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

import java.io.UnsupportedEncodingException;

import static cn.ljobin.bibi.netty.util.NettyCom.bytesToHexFun1;
import static org.apache.poi.util.HexDump.intToHex;


/**
 * @program: guns
 * @description: 语音模块发送封装
 * @author: Mr.Liu
 * @create: 2019-10-21 12:26
 **/
public class VoiceWriteAndFlush {
    /**头部**/
    private static final String HEAD = "FD";
    /**查询状态的指令**/
    private static final String STATUS = "FD000121";
    /**睡眠，省电模式**/
    private static final String SLEEP = "FD000188";
    /**从睡眠，省电模式唤醒设备**/
    private static final String AWAKEN = "FD0001FF";
    /**
     * 语音模块语句发送封装
     * @param channel 语音模块的com通道
     * @param msg 要发送的语音 小于4k
     */
    public static boolean voiceWriteAndFlush(Channel channel,String msg){
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return false;
        }
        try {
            String data = bytesToHexFun1(msg.getBytes("GB2312"));
            int length = data.length()/2+2;
            String bibi;
            if(length/256>1){
                bibi = intToHex(length/256)+intToHex(length%256);
            }else {
                bibi= "00"+intToHex(length);
            }
            byte[] bytes = ByteUtil.hexStringToBytes(HEAD+bibi+"0100"+data);
            ByteBuf buffer = channel.alloc().buffer();
            ByteBuf byteBuf = buffer.writeBytes(bytes);
            channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()) {
                        System.out.println("发送数据到语音模块成功");
                    } else {
                        System.out.println("发送数据到语音模块失败");

                    }
                }
            });
            return true;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * 语音模块 状态查询
     * @param channel 语音模块的com通道
     *  返回 0x4E 表示芯片正在合成中
     *  返回 0x4F 表示芯片处于空闲状态
     */
    public static boolean voiceGetStatus(Channel channel){
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return false;
        }
        byte[] bytes = ByteUtil.hexStringToBytes(STATUS);
        ByteBuf buffer2 = channel.alloc().buffer();
        ByteBuf byteBuf = buffer2.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("状态查询执行成功");
                } else {
                    System.out.println("状态查询执行失败");

                }
            }
        });
        return true;
    }
    /**
     * 语音模块 睡眠，省电模式
     * @param channel 语音模块的com通道
     */
    public static boolean voiceSleep(Channel channel){
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return false;
        }
        byte[] bytes = ByteUtil.hexStringToBytes(SLEEP);
        ByteBuf buffer2 = channel.alloc().buffer();
        ByteBuf byteBuf = buffer2.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("省电模式执行成功");
                } else {
                    System.out.println("省电模式执行失败");

                }
            }
        });
        return true;
    }
    /**
     * 语音模块 唤醒
     * @param channel 语音模块的com通道
     */
    public static boolean voiceAwaken(Channel channel){
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return false;
        }
        byte[] bytes = ByteUtil.hexStringToBytes(AWAKEN);
        ByteBuf buffer2 = channel.alloc().buffer();
        ByteBuf byteBuf = buffer2.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("唤醒成功");
                } else {
                    System.out.println("唤醒失败");

                }
            }
        });
        return true;
    }
}
