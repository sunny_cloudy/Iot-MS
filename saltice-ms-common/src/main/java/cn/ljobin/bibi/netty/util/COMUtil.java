package cn.ljobin.bibi.netty.util;

import gnu.io.CommPortIdentifier;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * @program: guns
 * @description: 获取电脑连接的com 口信息
 * @author: Mr.Liu
 * @create: 2019-10-17 19:57
 **/
public class COMUtil {
    /**
     * 获取所有串口名字
     */
    public static List<String> listPortChoices2() {
        CommPortIdentifier portId;
        Enumeration en = CommPortIdentifier.getPortIdentifiers();
        List<String> com = new ArrayList<>();
        // iterate through the ports.
        while (en.hasMoreElements()) {
            portId = (CommPortIdentifier) en.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                com.add(portId.getName());
                System.err.println(portId.getName());
            }
        }
        return com;
    }
    public static void main(String[] args){
        listPortChoices2();
    }
}
