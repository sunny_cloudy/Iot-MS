package cn.ljobin.bibi.netty.util;

import java.util.List;

public class ListUtil {

    /**
     * 填充浮点类型数据
     * @param list
     * @param start 开始填充的index
     * @param count 填充浮点数的个数
     * @param value 值
     */
    public  static void fill(List<String> list,int start,int count,String value){
        for (int i = start; i <start+count ; i++) {
            set(list,i,value);
        }
    }

    /**
     * 填充浮点类型数据
     * @param list
     * @param start 开始填充的index
     * @param count 填充浮点数的个数,一个浮点数据占用两个空间；所以 填充浮点数的个数 = 所占用空间 / 2；如：要填充8个浮点数，这个参数应该为16；
     * @param value 值
     */
    public static void fillFloat(List<String> list,int start,int count,String value){
        for (int i = start; i <start+count ; i+=2) {
            setFloat(list,i,value);
        }
    }

    /**
     * 填充布尔类型数据
     * @param list
     * @param start 开始填充的index
     * @param count 填充布尔数据的个数
     * @param value 值 1为异常；0为正常
     */
    public static void fillBollean(List<String> list,int start,int count,int value){
        for (int i = start; i <start+count ; i++) {
            setBoolean(list,i,value);
        }
    }

    public static <T> void set(List<T> list,int index,T value){
        list.set(index,value);
    }

    /**
     * 设置浮点数。一个浮点数占两个位置
     * @param list
     * @param start 该浮点数占用的一个位置的index
     * @param value 浮点数的16进制字符串
     */
    public static void setFloat(List<String> list,int start,String value){
        set(list,start,value.substring(0,4));
        set(list,start+1,value.substring(4,8));
    }

    /**
     * 设置浮点数。一个浮点数占两个位置
     * @param list
     * @param start 该浮点数占用的一个位置的index，用16进制的字符表示
     * @param value Float类型的浮点数
     */
    public  static void setFloat(List<String> list,String start,Float value){
        int startInt = ByteUtil.hexStringToNum(start);
        String hexString = ByteUtil.floatToHexString(value);
        setFloat(list,startInt,hexString);
    }

    /**
     * 设置浮点数。一个浮点数占两个位置
     * @param list
     * @param start 该浮点数占用的一个位置的index，用16进制的字符表示
     * @param value 浮点数的16进制字符串
     */
    public  static void setFloat(List<String> list,String start,String value){
        int startInt = ByteUtil.hexStringToNum(start);
        setFloat(list,startInt,value);
    }

    /**
     * 设置布尔类型数据。1为异常；0为正常
     * @param list
     * @param index 表示list的下标
     * @param value
     */
    public static void setBoolean(List<String> list,int index,int value){
        String lengthHexString = ByteUtil.numToLengthHexString(value, 2);
        set(list,index,lengthHexString);
    }
    /**
     * 设置布尔类型数据。
     * @param list
     * @param index 16进制的字符，表示list的下标
     * @param value 1为异常；0为正常
     */
    public static void setBoolean(List<String> list,String index,int value){
        String lengthHexString = ByteUtil.numToLengthHexString(value, 2);
        int indexInt = ByteUtil.hexStringToNum(index);
        set(list,indexInt,lengthHexString);
    }

    /**
     * 设置布尔类型数据。
     * @param list
     * @param index 16进制的字符，表示list的下标
     * @param value 1为异常；0为正常
     */
    public static void setBoolean(List<String> list,String index,String value){
        int indexInt = ByteUtil.hexStringToNum(index);
        set(list,indexInt,value);
    }



}
