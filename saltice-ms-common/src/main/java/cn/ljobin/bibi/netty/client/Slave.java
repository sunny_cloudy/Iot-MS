package cn.ljobin.bibi.netty.client;


import cn.ljobin.bibi.netty.decoder.PacketDecoder;
import cn.ljobin.bibi.netty.handler.RxtxHandler;
import cn.ljobin.bibi.netty.util.ByteUtil;
import cn.ljobin.bibi.netty.util.CRC16Util;
import cn.ljobin.bibi.utils.cache.Cache;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.oio.OioEventLoopGroup;
import io.netty.channel.rxtx.RxtxChannel;
import io.netty.channel.rxtx.RxtxChannelConfig;
import io.netty.channel.rxtx.RxtxDeviceAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Scanner;

public class Slave {
    private static final Logger logger = LoggerFactory.getLogger(Slave.class);
    private String portName;
    private int baudrate;
    RxtxChannel channel;
    public List<String> data;
    private ChannelFuture future;
    private OioEventLoopGroup group;
    private Bootstrap b;
    private String uid;

    public Slave(String portName, int baudrate, List<String> data, String uid) {
        this.portName = portName;
        this.baudrate = baudrate;
        this.data = data;
        this.uid = uid;
    }

    public Slave(String portName, int baudrate, List<String> data) {
        this.data=data;
        this.portName = portName;
        this.baudrate = baudrate;
    }

    public void run(){

        try {
            group = new OioEventLoopGroup();
            b = new Bootstrap();
            b.group(group)
                    .channelFactory(new ChannelFactory<RxtxChannel>() {
                        public RxtxChannel newChannel() {
                            return channel;
                        }
                    })
                    .handler(new ChannelInitializer<RxtxChannel>() {
                        @Override
                        public void initChannel(RxtxChannel ch) throws Exception {
                            ch.pipeline().addLast(
                                    //固定的长度就是如： A5 5A 01 06 01 03 03 03 00 32 36 C3
                                    //new FixedLengthFrameDecoder(12),
                                    new PacketDecoder(),
                                    new RxtxHandler(data,uid)
                            );
                        }
                    });

            channel = new RxtxChannel();
            channel.config().setBaudrate(baudrate);
            channel.config().setDatabits(RxtxChannelConfig.Databits.DATABITS_8);
            channel.config().setParitybit(RxtxChannelConfig.Paritybit.NONE);
            channel.config().setStopbits(RxtxChannelConfig.Stopbits.STOPBITS_1);
            Cache.put("comChannel",channel);
            future = b.connect(new RxtxDeviceAddress(portName)).sync();
            Channel channels = (Channel) Cache.get("channel");
            if (channels!=null){
                RxtxHandler.writeAndFlushNettyCom(channels, "client",true);
            }
        } catch (InterruptedException e) {
            logger.error("com Run Error：{}",e.getMessage());
            group.shutdownGracefully();
            b = null;
            Channel channels = (Channel) Cache.get("channel");
            if (channels!=null){
                RxtxHandler.writeAndFlushNettyCom(channels,"client",false);
            }
            logger.info("closed com application");
        }
    }

    public void stop(){
            if(this.channel.isActive()||this.channel.isOpen()){
                this.channel.close();
                this.group.shutdownGracefully();
            }
        Channel channels = (Channel) Cache.get("channel");
        if (channels!=null){
            RxtxHandler.writeAndFlushNettyCom(channels,RxtxHandler.DID,false);
        }
        logger.info("You closed com application");
    }

    public void writeAndFlush(String hexString) {
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return;
        }
        String s = "A55A01"+ CRC16Util.xorDeal(hexString)+"C3";
        System.err.println("手动发送数据："+s);
        byte[] bytes = ByteUtil.hexStringToBytes(s);
        ByteBuf buffer = this.channel.alloc().buffer();
        ByteBuf byteBuf = buffer.writeBytes(bytes);
        this.channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                StringBuilder sb = new StringBuilder("");
                if (future.isSuccess()) {
                    System.out.println(sb.toString() + "回写成功");
                } else {
                    System.out.println(sb.toString() + "回写失败");

                }
            }
        });
    }

    public void inputCmd(){
        Scanner sc=new Scanner(System.in);
        while (true){
            String s = sc.nextLine();
            writeAndFlush(s);
        }
    }

    public void request() throws InterruptedException {
        String cmd1 = "030300000002";
        String cmd2 = "020300020001";
        String cmd3 = "040300030001";
        Thread.sleep(1000);
        writeAndFlush(cmd1);
        Thread.sleep(1000);
        writeAndFlush(cmd2);
        Thread.sleep(1000);
        writeAndFlush(cmd3);
    }


}
