package cn.ljobin.bibi.netty.util.fileupload;

import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

//常量
public class bConstant {
    /** 文件上传/图片   根目录 */
    public String UPLOAD_PATH = "";
    {
        try {
            File path = new File(ResourceUtils.getURL("classpath:").getPath());
            UPLOAD_PATH = path.getParentFile().getParentFile().getParent() + File.separator + "apache-tomcat-9.0.17" + File.separator+"webapps"+File.separator+"ROOT"+File.separator;
            System.out.println(UPLOAD_PATH);
            /*
             * 上线就加上下面的代码*
             *项目放在tomcat中则不要加下面这句话，它会保存在
             * /developer/apache-tomcat-9.0.17/webapps/apache-tomcat-9.0.17/webapps/ROOT/iotImg里面
             * jar包运行项目则加上就行 它会保存在/developer/apache-tomcat-9.0.17/webapps/ROOT/iotImg
             */
            /*UPLOAD_PATH = UPLOAD_PATH.substring(5,UPLOAD_PATH.length());*/
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /** 图片目录 */
    public static final String IMG_FILE_NAME = "excel";

    /** 图片相对路径 */
    public static final String VIRTUAL_IMG_PATH = "excel/virtual";
}
