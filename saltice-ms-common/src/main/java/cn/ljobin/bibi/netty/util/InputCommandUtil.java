package cn.ljobin.bibi.netty.util;

public class InputCommandUtil {
    /**
     * 获取地址码
     */

    public static String getAddrCode(String cmd){
        return cmd.substring(0,2);
    }
    /**
     * 获取头部信息
     */
    public static String getHeadCode(String cmd){
        return cmd.substring(0,4);
    }
    /**
     * 获取尾部信息
     */
    public static String getEndCode(String cmd){
        int length = cmd.length();
        return cmd.substring(length-2,length);
    }
    /**
     * 获取尾部信息 后面三个
     */
    public static String getEndCode3(String cmd){
        int length = cmd.length();
        return cmd.substring(length-6,length);
    }
    /**
     *获取命令请求的数据起始地址
     */
    public static String getStarAddr(String cmd){
        return cmd.substring(4,8);
    }
    /**
     *获取命令请求的数据起始地址
     */
    public static int getStarAddrInt(String cmd){
        String starAddr = getStarAddr(cmd);
        return ByteUtil.hexStringToNum(starAddr);
    }
    /**
     *获取命令请求的控制符 02 03啥的
     */
    public static int getControlCodeInt(String cmd){
        String starAddr = getControlCode(cmd);
        return ByteUtil.hexStringToNum(starAddr);
    }
    /**
     *获取命令请求的数据域 （药柜号（1个字节）+行号（1个字节）
     * +列号（1个字节）+格内号（1个字节）+药剂量（两个字节））
     */
    public static int[] getDataCodeInt(String cmd){
        String length = getDataLengthCode(cmd);
        String starAddr = getDataCode(cmd,ByteUtil.hexStringToNum(length));
        if(starAddr.length()%2!=0)return new int[0];
        StringBuffer s1 = new StringBuffer(starAddr);
        int index;
        for (index = 2; index < s1.length(); index += 3) {
            s1.insert(index, ',');
        }
        String[] array = s1.toString().split(",");
        int arrayLength = array.length;
        int[] data = new int[arrayLength];
        for (int i=0;i<arrayLength;i++) {
            data[i] = ByteUtil.hexStringToNum(array[i]);
        }
        return data;
    }
    /**
     *获取命令请求的数据的个数
     */
    public static String getReqDataCount(String cmd){
        return cmd.substring(8,12);
    }
    /**
     *获取命令请求的数据的个数
     */
    public static int getReqDataCountInt(String cmd){
        String reqDataCount = getReqDataCount(cmd);
        return ByteUtil.hexStringToNum(reqDataCount);
    }
    public static boolean isAddrCode01(String cmd){
        if(cmd.length() < 2) return false;
        return "01".equals(getAddrCode(cmd));
    }
    /**
     * 获取crc检验
     * @param cmd
     * @return
     */
    public static String getCrc(String cmd){
        return cmd.substring(12,16);
    }

    /**
     * 获取 控制码
     * @param cmd
     * @return
     */
    public static String getControlCode(String cmd) {
        return cmd.substring(4,6);
    }
    /**
     * 获取 数据域长度字段
     * @param cmd
     * @return
     */
    public static String getDataLengthCode(String cmd) {
        return cmd.substring(6,8);
    }
    /**
     * 获取 数据域
     * @param cmd
     * @return
     */
    public static String getDataCode(String cmd,int length) {
        return cmd.substring(8,length*2+8);
    }
    /**
     * 判断 控制码 是否是01 02 03
     * @param cmd
     * @return
     */
    public static boolean isControlCode01(String cmd){
        String tag = getControlCode(cmd);
        return "01".equals(tag)||"02".equals(tag)||"03".equals(tag)||"04".equals(tag);
    }
    /**
     * 判断 控制码 是否是00 表示后台发送的数据
     * @param cmd
     * @return
     */
    public static boolean isControlCode00(String cmd){
        return "00".equals(getControlCode(cmd));
    }

    /**
     * 获判断头部信息 是否是A55A
     * @param cmd
     * @return
     */
    public static boolean isHeadCodeA55A(String cmd){
        return "A55A".equals(getHeadCode(cmd))||"a55a".equals(getHeadCode(cmd));
    }
    /**
     * 获判断头部信息 是否是AA 55
     * @param cmd
     * @return
     */
    public static boolean isHeadCodeAA55(String cmd){
        return "AA55".equals(getHeadCode(cmd))||"aa55".equals(getHeadCode(cmd));
    }
    /**
     * 判断尾部信息 是否是C3
     * @param cmd
     * @return
     */
    public static boolean isEndCodeC3(String cmd){
        return "C3".equals(getEndCode(cmd))||"c3".equals(getEndCode(cmd));
    }
    /**
     * 判断尾部信息 是否是 55 AA 40
     * @param cmd
     * @return
     */
    public static boolean isEndCodeClient(String cmd){
        return "55AA40".equals(getEndCode3(cmd))||"55AA40".equals(getEndCode3(cmd));
    }
    /**
     * 后台采用这个
     * @param cmd
     * @return
     */
    public static boolean isMasterCmd(String cmd){
        if(cmd.length() < 6) return false;
        // && cmd.length() == 14 上线的时候加上
        return isHeadCodeA55A(cmd) && isControlCode00(cmd) && isEndCodeC3(cmd) ;
    }

    /**
     * 串口用这个
     * @param msg
     * @return
     */
    public static boolean isSlaveCmd(String msg){
        if(msg.length() < 6) return false;
        // && msg.length() == 14  上线的时候加上
        return isHeadCodeA55A(msg) && isControlCode01(msg) && isEndCodeC3(msg);
    }
    /**
     * 服务器客户端用这个
     * @param msg
     * @return
     */
    public static boolean isClientCmd(String msg){
        if(msg.length() < 14) return false;
        // && msg.length() == 14  上线的时候加上
        return isHeadCodeAA55(msg) && isEndCodeClient(msg);
    }
    public static void main(String[] args) {
        String cmd = "010300010003aaaa";
        int starAddrInt = InputCommandUtil.getStarAddrInt(cmd);
        System.out.println(starAddrInt);

        int dataCountInt = InputCommandUtil.getReqDataCountInt(cmd);
        System.out.println(dataCountInt);

        String msg="03030402DE00F239F402030200013D84";
        boolean masterCmd = InputCommandUtil.isMasterCmd(msg);
        boolean slaveCmd = InputCommandUtil.isSlaveCmd(msg);

        if (InputCommandUtil.isMasterCmd(msg) || InputCommandUtil.isSlaveCmd(msg)) {
            //System.out.println("");
        }else{

            System.out.println("丢弃一条数据");
        }

    }
}
