package cn.ljobin.bibi.netty.handler;


import cn.ljobin.bibi.model.MStream;
import cn.ljobin.bibi.netty.util.*;
import cn.ljobin.bibi.netty.voice.VoiceWriteAndFlush;
import cn.ljobin.bibi.utils.cache.Cache;
import cn.ljobin.bibi.utils.httpclient.HttpClientUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static cn.ljobin.bibi.netty.client.Voice.VOICENAME;
import static org.apache.poi.util.HexDump.intToHex;

public class RxtxHandler  extends ChannelInboundHandlerAdapter {
    /**用来保存取药的 <uid:list<String>>
     * 在发送数据的时候才进行转换为【药柜号（1个字节）+行号（1个字节）+列号（1个字节）+格内号（1个字节）+药剂量（两个字节）】
     * 按十六进制保持 如 01 03 03 03 00 32
     **/
    public static final ConcurrentMap<String,List<String>> KFMAP = new ConcurrentHashMap<>();
    private static AtomicInteger index = new AtomicInteger(0);
    /**用来判断一个字符串能否转化为int**/
    public static final Pattern PATTERN = Pattern.compile("0|([-]?[1-9][0-9]*)");
    public static final Pattern INT = Pattern.compile("\\d+");
    public static boolean isInt(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    private List<String> data;
    private String uid = "";
    public static final String DID =  UUID.randomUUID().toString().replace("-","")+ System.currentTimeMillis();
    public static int getIndex() {
        return index.get();
    }

    public static void setIndex(int index) {
        RxtxHandler.index.set(index);
    }

    public RxtxHandler(List<String> data, String uid) {
        this.data = data;
        this.uid = uid;
    }


    /**
     * 接收到客户端数据的回调
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception{
        if(msg instanceof String ){
            String packet = (String)msg;
            //如果控制码是00 ,表示为上位机发来的请求命令
            if(InputCommandUtil.isMasterCmd(packet)){
                System.out.println("获取服务器请求："+packet);
                int starAddr = InputCommandUtil.getControlCodeInt(packet);
                int[] dataCountInt = InputCommandUtil.getDataCodeInt(packet);
                List<String> strings = data.subList(starAddr, starAddr + dataCountInt.length);
                strings.forEach(System.out::println);
                String resCommand = OutputCommandUtil.packet(strings);
                writeAndFlushCom(ctx.channel(),resCommand);

            }else{
                //否则是从传感器发来的响应命令
                if(InputCommandUtil.isSlaveCmd(packet)){
                    //其它则表示从下位机取的数据
                    System.out.println("获取硬件数据："+packet);
                    if(DecodeCommandUtil.isNextCode(packet)){
                        int[] val = InputCommandUtil.getDataCodeInt(packet);
                        Channel channel = (Channel)Cache.get("channel");
                        List<String> list = KFMAP.get(uid);
                        if(list==null||list.isEmpty()){
                            writeAndFlushComError(ctx.channel());
                        }else {
                            int in = -1;
                            if(index.get()>=list.size()){
                                in = list.size()-1;
                            }else {
                                in = index.getAndIncrement();
                            }
                            if (channel != null) {
                                writeAndFlushNetty(channel,uid,"1",in);
                            }
                            String code = list.get(in);
                            int length = code.length()/2;
                            String len = ByteUtil.numToLengthHexString(length,2).toUpperCase();
                            writeAndFlushCom(ctx.channel(),len+code);
                        }

                    }else if(DecodeCommandUtil.isPreCode(packet)){
                        int[] val = InputCommandUtil.getDataCodeInt(packet);
                        Channel channel = (Channel)Cache.get("channel");
                        List<String> list = KFMAP.get(uid);
                        if(list==null||list.isEmpty()){
                            writeAndFlushComError(ctx.channel());
                        }else {
                            int in = 0;
                            if(index.get()<=0){
                                in = 0;
                            }else {
                                in = index.decrementAndGet();
                            }
                            if (channel != null) {
                                writeAndFlushNetty(channel,uid,"0",in);
                            }
                            String code = list.get(in);
                            int length = code.length()/2;
                            String len = ByteUtil.numToLengthHexString(length,2).toUpperCase();
                            writeAndFlushCom(ctx.channel(),len+code);
                        }

                    }else if(DecodeCommandUtil.isNextMan(packet)){
                        String uid = "client";
                        if("".equals(uid) || uid == null){
                            System.err.println("没有获取到账号信息，请重新前往web页面开启COM");
                            //SendQQMailUtil.sendMailQQ("没有获取到账号信息","请重新前往web页面开启COM");
                        }
                        JSONObject jsonObject =  HttpClientUtil.httpClientGet("http://47.103.1.210:8082/guns-1.0.0/h5/kf_yao_getByid_loc/"+uid);
                        if (jsonObject != null && "200".equals(jsonObject.getString("code"))) {
                            JSONArray ents = jsonObject.getJSONArray("data");
                            List<MStream> listThickness = getResultForArray(ents.toJSONString(), MStream.class);
                            //保存药品数据的集合
                            List<String> kfCode = new ArrayList<>();
                            int kfsLength=listThickness.size();
                            for (int i = 0; i < kfsLength; i++) {
                                StringBuilder kfBuilder = new StringBuilder("");
                                String box = listThickness.get(i).getTips().split("-->")[0];
                                String box_n = box.substring(box.length()-1);
                                String x = listThickness.get(i).getTips().split("-->")[1].split("，")[0];
                                String x_x = x.substring(1,x.length()-1);;
                                String y = listThickness.get(i).getTips().split("-->")[1].split("，")[1];
                                String y_y = y.substring(1,y.length()-1);
                                String geNei = intToHex(1);
                                String dose = "";
                                if(isInt(listThickness.get(i).getDose())){
                                    int data = Integer.parseInt(listThickness.get(i).getDose());
                                    if(data>65536){
                                        data = 0;
                                    }
                                    if(data/256>1){
                                        dose = intToHex(data/256)+intToHex(data%256);
                                    }else {
                                        dose= "00"+intToHex(data);
                                    }
                                }else {
                                    Matcher m = INT.matcher(listThickness.get(i).getDose());
                                    m.find();
                                    int data = Integer.parseInt(m.group());
                                    if(data>65536){
                                        data = 0;
                                    }
                                    if(data/256>1){
                                        dose = intToHex(data/256)+intToHex(data%256);
                                    }else {
                                        dose= "00"+intToHex(data);
                                    }
                                }
                                kfBuilder.append(intToHex(Integer.parseInt(box_n)))
                                        .append(intToHex(Integer.parseInt(x_x)))
                                        .append(intToHex(Integer.parseInt(y_y)))
                                        .append(geNei)
                                        .append(dose);
                                kfCode.add(kfBuilder.toString());
                            }
                            KFMAP.put(jsonObject.getString("message"),kfCode);
                            index.set(0);
                            writeAndFlushComOk(ctx.channel());
                        }else {
                            writeAndFlushComError(ctx.channel());
                            //SendQQMailUtil.sendMailQQ("访问异常","请联系管理员");
                        }
                    } else{
                        writeAndFlushComError(ctx.channel());
                        //SendQQMailUtil.sendMailQQ("指令错误","请检查发送指令");
                    }
                }else{
                    //数据不满足传感器的数据格式
                    System.err.println("一条异常数据："+packet);
                    writeAndFlushComError(ctx.channel());
                }
            }
        }


    }
    /**
     * 将数字转为2位的16机制字符串
     * @param n
     * @return
     */
    public static String intToHex(int n) {
        return ByteUtil.numToLengthHexString(n,2).toUpperCase();
    }
    public static <T> List<T> getResultForArray(String result, Class<T> clazz) {
        List<T> re = null;
        try {
            if (result != null && result.length() > 0) {
                re = JSONArray.parseArray(result, clazz);
            }
        } catch (Exception e) {
            System.err.println("处理jsonArray失败");
        }
        return re;
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    /**
     * 发送数据到netty服务器
     * @param channel
     * @param hexString 对应的账号
     * @param controlCode  控制信息，上一个还是下一个
     * @param index 当前取第几个了
     */
    public static void writeAndFlushNetty(Channel channel,String hexString,String controlCode,int index) {
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return;
        }
        String s = "AA55"+ByteUtil.str2HexStr("adiv112233:"+hexString+":"+controlCode+":"+index)+"55AA40";
        byte[] bytes = ByteUtil.hexStringToBytes(s);
        ByteBuf buffer = channel.alloc().buffer();
        ByteBuf byteBuf = buffer.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("发送【控制信息】数据到netty服务器成功: "+s);
                } else {
                    System.out.println("发送【控制信息】数据到netty服务器失败: "+s);

                }
            }
        });
    }
    /**
     * 发送响应心跳包数据到netty服务器
     * @param channel
     */
    public static void writeAndFlushNettyHeart(Channel channel) {
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return;
        }
        String s = "AA55"+ByteUtil.str2HexStr("adiv112233:H")+"55AA40";
        byte[] bytes = ByteUtil.hexStringToBytes(s);
        ByteBuf buffer = channel.alloc().buffer();
        ByteBuf byteBuf = buffer.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("发送【心跳】数据到netty服务器成功: "+s);
                } else {
                    System.out.println("发送【心跳】数据到netty服务器失败: "+s);

                }
            }
        });
    }
    /**
     * 发送数据到netty服务器 并通知netty服务器绑定保存改通道
     * @param channel
     * @param hexString 对应的账号
     */
    public static void writeAndFlushNettyAndUid(Channel channel,String hexString) {
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return;
        }
        String s = "AA55"+ByteUtil.str2HexStr("adiv112233:"+ DID +":"+hexString+":-1")+"55AA40";
        byte[] bytes = ByteUtil.hexStringToBytes(s);
        ByteBuf buffer = channel.alloc().buffer();
        ByteBuf byteBuf = buffer.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("发送【注册】数据到netty服务器成功: "+s);
                } else {
                    System.out.println("发送【注册】数据到netty服务器失败: "+s);

                }
            }
        });
    }
    /**
     * 发送数据到netty服务器 并通知netty服务器 保存本地com应用的com监听程序的状态
     * @param channel
     * @param uid 对应的账号
     * @param tag true:启动成功(y) false:失败(n)
     */
    public static void writeAndFlushNettyCom(Channel channel, String uid, boolean tag) {
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return;
        }
        String s = "AA55"+ ByteUtil.str2HexStr("adiv112233:"+ DID +":"+uid+":"+(tag?"y":"n")+"")+"55AA40";
        byte[] bytes = ByteUtil.hexStringToBytes(s);
        ByteBuf buffer = channel.alloc().buffer();
        ByteBuf byteBuf = buffer.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("发送【注册com监听程序的状态】数据到netty服务器成功: "+s);
                } else {
                    System.out.println("发送【注册com监听程序的状态】数据到netty服务器失败: "+s);

                }
            }
        });
    }
    /**
     * 发送数据到netty服务器 并通知netty服务器 通知本地应用程序立马发送取下一个药的指令
     * @param channel
     * @param uid 对应的账号
     */
    public static void writeAndFlushNettyUpdateKf(Channel channel,String uid) {
        if(!channel.isActive() || !channel.isOpen() || !channel.isWritable()){
            return;
        }
        String s = "AA55"+ByteUtil.str2HexStr("adiv112233:updateDivKf:"+uid+":1")+"55AA40";
        byte[] bytes = ByteUtil.hexStringToBytes(s);
        ByteBuf buffer = channel.alloc().buffer();
        ByteBuf byteBuf = buffer.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("发送【通知】数据到netty服务器成功: "+s);
                } else {
                    System.out.println("发送【通知】数据到netty服务器失败: "+s);
                }
            }
        });
    }
    /**
     * 发送药品数据到comm口
     * @param channel
     * @param hexString
     */
    public static void writeAndFlushCom(Channel channel,String hexString) {
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return;
        }
        Channel voiceChannel = (Channel) Cache.get(VOICENAME);
        if (voiceChannel != null && voiceChannel.isWritable()){
            String[] data = hexString.substring(2).replaceAll("(\\w{2})(?=.)","$1,").split(",");
            if(data.length == 6){
                String stringBuilder = "" + "第" +
                        Long.parseLong(data[0], 16)+
                        "号药柜的第" +
                        Long.parseLong(data[1], 16) +
                        "航第" +
                        Long.parseLong(data[2], 16) +
                        "列，" +
                        "格内号为" +
                        Long.parseLong(data[3], 16) +
                        "，剂量为" +
                        Long.parseLong(data[4]+data[5], 16) + "克。";
                VoiceWriteAndFlush.voiceWriteAndFlush(voiceChannel, stringBuilder);
            }

        }
        System.err.println("发送："+hexString);
        String s = "A55A01"+ CRC16Util.xorDeal(hexString)+"C3";
        byte[] bytes = ByteUtil.hexStringToBytes(s);
        ByteBuf buffer = channel.alloc().buffer();
        ByteBuf byteBuf = buffer.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("发送【药品】数据到comm口成功: "+s);
                } else {
                    System.out.println("发送【药品】数据到comm口失败: "+s);

                }
            }
        });
    }
    /**
     * 发送确认数据到comm口
     * @param channel
     */
    public static void writeAndFlushComOk(Channel channel) {
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return;
        }
        String s = "A55A05"+CRC16Util.xorDeal("01E0")+"C3";
        byte[] bytes = ByteUtil.hexStringToBytes(s);
        ByteBuf buffer = channel.alloc().buffer();
        ByteBuf byteBuf = buffer.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("发送【确认】数据到comm口成功: "+s);
                } else {
                    System.out.println("发送【确认】数据到comm口失败: "+s);

                }
            }
        });
    }
    /**
     * 发送错误数据到comm口
     * @param channel
     */
    public static void writeAndFlushComError(Channel channel) {
        if(!channel.isActive() || !channel.isOpen()|| !channel.isWritable()){
            return;
        }
        String s = "A55A06"+CRC16Util.xorDeal("01FE")+"C3";
        byte[] bytes = ByteUtil.hexStringToBytes(s);
        ByteBuf buffer = channel.alloc().buffer();
        ByteBuf byteBuf = buffer.writeBytes(bytes);
        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    System.out.println("发送【错误】数据到comm口成功: "+s);
                } else {
                    System.out.println("发送【错误】数据到comm口失败: "+s);

                }
            }
        });
    }
}
