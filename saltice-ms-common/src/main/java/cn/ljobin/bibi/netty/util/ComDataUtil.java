package cn.ljobin.bibi.netty.util;

import gnu.io.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.TooManyListenersException;

/**
 * @program: guns
 * @description: 串口数据接收
 * @author: Mr.Liu
 * @create: 2019-10-05 19:43
 **/
public class ComDataUtil implements SerialPortEventListener {

    /**通讯端口标识符**/
    protected static CommPortIdentifier portid = null;
    /**串行端口**/
    protected static SerialPort comPort = null;
    /**波特率**/
    protected static int BAUD = 115200;
    /**数据位**/
    protected static int DATABITS = SerialPort.DATABITS_8;
    /**停止位**/
    protected static int STOPBITS = SerialPort.STOPBITS_1;
    /**奇偶检验**/
    protected static int PARITY = SerialPort.PARITY_NONE;
    /**输出流**/
    public static OutputStream oStream;
    /**输入流**/
    public static InputStream iStream;

    /**
     * 读取所有串口名字
     */
    public static void listPortChoices() {
        CommPortIdentifier portId;
        Enumeration en = CommPortIdentifier.getPortIdentifiers();
        // iterate through the ports.
        while (en.hasMoreElements()) {
            portId = (CommPortIdentifier) en.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                System.out.println(portId.getName());
            }
        }
    }

    public static boolean checkCom(String comName){
        String osName = null;
        String osname = System.getProperty("os.name", "").toLowerCase();
        if (osname.startsWith("windows")) {
            // windows
            osName = comName;
        } else if (osname.startsWith("linux")) {
            // linux
            osName = "/dev/ttyS1";
        }
        System.out.println(osName);
        try {
            portid = CommPortIdentifier.getPortIdentifier(osName);
            if (portid.isCurrentlyOwned()) {
                System.out.println("端口在使用");
                return false;
            }
        } catch (NoSuchPortException e) {
            System.out.println("端口不存在");
            return false;
        }
        return true;
    }
    /**
     * 设置串口号
     * 获取数据
     * @param
     * @return
     */
    public void setSerialPortNumber() {

        String osName = null;
        String osname = System.getProperty("os.name", "").toLowerCase();
        if (osname.startsWith("windows")) {
            // windows
            osName = "COM5";
        } else if (osname.startsWith("linux")) {
            // linux
            osName = "/dev/ttyS1";
        }
        System.out.println(osName);
        try {
            portid = CommPortIdentifier.getPortIdentifier(osName);
            // portid = CommPortIdentifier.getPortIdentifier(Port);
            if (portid.isCurrentlyOwned()) {
                System.out.println("端口在使用");
            } else {
                //this.getClass().getName()
                comPort = (SerialPort) portid.open(this.getClass().getName(), 1000);
            }
        } catch (PortInUseException e) {
            System.out.println("端口被占用");
            e.printStackTrace();

        } catch (NoSuchPortException e) {
            System.out.println("端口不存在");
            e.printStackTrace();
        }



        try {
            //设置串口参数依次为(波特率,数据位,停止位,奇偶检验)
            comPort.setSerialPortParams(BAUD, DATABITS, STOPBITS, PARITY);
        } catch (UnsupportedCommOperationException e) {
            System.out.println("端口操作命令不支持");
            e.printStackTrace();
        }
        try {
            //给当前串口增加一个监听器
            comPort.addEventListener(this);
            //当有数据时通知
            comPort.notifyOnDataAvailable(true);
            //System.out.print("有数据");
        } catch (TooManyListenersException e) {
            e.printStackTrace();
        }
        //记录已经到达串口COM4且未被读取的数据的字节（Byte）数。
        int availableBytes = 0;
        //定义用于缓存读入数据的数组
        byte[] cache = new byte[1024];
        try {
            //从COM口获取数据
            iStream = comPort.getInputStream();
            System.out.println("获取的数据对象："+iStream);
            while (true) {
                availableBytes = iStream.available();
                while (availableBytes>0) {
                    byte[] imgdata = new byte[1024];
                    //从串口的输入流对象中读入数据并将数据存放到缓存数组中
                    iStream.read(cache);
                    for(int i =0,j=0; i < cache.length && i < availableBytes; i++) {
                        if(BinaryToHexString(cache[i]).equals("A5")||BinaryToHexString(cache[i]).equals("a5")){
                            if(BinaryToHexString(cache[i+1]).equals("5A")||BinaryToHexString(cache[i+1]).equals("5A")){

                            }
                        }
                    }

                    /**
                     * 下面的32 33是通过数据推算出来的图片数据起始位置
                     * 要是用不同的格式照片，这个要改
                     * (cache[i-1] == -1 && cache[i]==-39)是图片数据结束位置
                     * **/
                    for(int i =32,j=0; i < cache.length&& i < availableBytes; i++) {

                        if(cache[32]==-1 && cache[33] ==-40)
                        {
                            imgdata[j++]=cache[i];
                        }
                        if((cache[32]!=-1 && cache[33] !=-40)||(cache[i-1] == -1 && cache[i]==-39))
                        {
                            break;
                        }
                    }

                    System.out.print("\n");

                    availableBytes = iStream.available();
                    /**
                     * 设置暂停的时间 5 秒，以便每一次能够接收到全部图片数据
                     * 可根据不同的单片机数据发送上来的时间决定
                     * **/
                    Thread.sleep(5*1000);
                }


            }
            // oStream = comPort.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 返回的String是String类型
     * @param bytes
     * @return
     */
    public static String BinaryToHexString(byte bytes){
        String hex;
        //字节高4位
        hex = String.valueOf((bytes&0xF0)>>4);
        //字节低4位
        hex += String.valueOf(bytes&0x0F);
        return hex;
    }
    @Override
    public void serialEvent(SerialPortEvent event) {
        switch (event.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                break;
            case SerialPortEvent.DATA_AVAILABLE:// 当有可用数据时读取数据,并且给串口返回数据
                //进来后 就不是第一次了
                //TODO
                break;
        }
    }
    public static void main(String[] args) {
//        ComDataUtil se = new ComDataUtil();
//        se.setSerialPortNumber();
        listPortChoices();
    }
}

