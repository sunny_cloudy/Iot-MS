package cn.ljobin.bibi.domain.ms;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 医嘱信息对象 advise
 * 
 * @author lyb
 * @date 2020-04-26
 */
@TableName("advise")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Advise
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** $column.columnComment */
    private String kfid;

    /** 用法 0：内服 1：外用 */
    private Integer way;

    /** 贴数 */
    private String num;

    /** 使用时间 */
    private String eatTime;

    /** 禁忌 */
    private String taboo;

    /** 医嘱 */
    private String advise;
}
