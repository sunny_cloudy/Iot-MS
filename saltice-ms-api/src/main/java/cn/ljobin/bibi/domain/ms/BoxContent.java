package cn.ljobin.bibi.domain.ms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @program: guns
 * @description:
 * @author: Mr.Liu
 * @create: 2020-02-24 14:16
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BoxContent {
        String boxName;
        String address_x;
        String address_y;
        String yaoName;
        // 1表示要取
        int tag;
        int order;


        public BoxContent(String boxName, String address_x, String address_y, String yaoName, int tag) {
            this.boxName = boxName;
            this.address_x = address_x;
            this.address_y = address_y;
            this.yaoName = yaoName;
            this.tag=tag;
        }
        public BoxContent(String boxName, String address_x, String address_y, String yaoName) {
            this.boxName = boxName;
            this.address_x = address_x;
            this.address_y = address_y;
            this.yaoName = yaoName;
            this.tag=0;
        }
    }
