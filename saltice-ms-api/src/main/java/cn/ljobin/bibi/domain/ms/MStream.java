package cn.ljobin.bibi.domain.ms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 【请填写功能名称】对象 m_stream
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("m_stream")
public class MStream
{

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 开方单编号 */
    private String kfid;

    /** 药品名称 */
    private String mName;

    /** 药量 */
    private String dose;

    /** 备注 */
    private String tips;

    /** 取药的状态 0：未取，1：已取 */
    private Long version;

}
