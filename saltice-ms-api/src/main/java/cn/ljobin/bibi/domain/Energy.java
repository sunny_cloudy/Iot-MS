package cn.ljobin.bibi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("tb_energy")
public class Energy {
    private static final long serialVersioncharge = 1L;
    @TableId(value = "id",type = IdType.AUTO)
    //字段名称和属性名称一致的可以不用这个注解
    private Long  id;

    private String imei;

    private String pow1;

    private String grouppow;

    private String vol18v1;

    private String vol18v2;

    private String vol24;

    private String vol28;

    private String extendpow;

    private String solarpow;

    private String operating_temp;

    private String charge;

    private Integer work;

    private String colltime;

    @TableField(exist=false)
    //在实体类中有一个属性为endtime，但是在数据库中没有这个字段，就添加这个注解
    private String endtime;

    private String deptid;

    @TableField(exist=false)
    private String status;

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public static long getSerialVersioncharge() {
        return serialVersioncharge;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPow1() {
        return pow1;
    }

    public void setPow1(String pow1) {
        this.pow1 = pow1;
    }

    public String getGrouppow() {
        return grouppow;
    }

    public void setGrouppow(String grouppow) {
        this.grouppow = grouppow;
    }

    public String getVol18v1() {
        return vol18v1;
    }

    public void setVol18v1(String vol18v1) {
        this.vol18v1 = vol18v1;
    }

    public String getVol18v2() {
        return vol18v2;
    }

    public void setVol18v2(String vol18v2) {
        this.vol18v2 = vol18v2;
    }

    public String getVol24() {
        return vol24;
    }

    public void setVol24(String vol24) {
        this.vol24 = vol24;
    }

    public String getVol28() {
        return vol28;
    }

    public void setVol28(String vol28) {
        this.vol28 = vol28;
    }

    public String getExtendpow() {
        return extendpow;
    }

    public void setExtendpow(String extendpow) {
        this.extendpow = extendpow;
    }

    public String getSolarpow() {
        return solarpow;
    }

    public void setSolarpow(String solarpow) {
        this.solarpow = solarpow;
    }

    public String getOperating_temp() {
        return operating_temp;
    }

    public void setOperating_temp(String operating_temp) {
        this.operating_temp = operating_temp;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public Integer getWork() {
        return work;
    }

    public void setWork(Integer work) {
        this.work = work;
    }

    public String getColltime() {
        return colltime;
    }

    public void setColltime(String colltime) {
        this.colltime = colltime;
    }

    public String getDeptid() {
        return deptid;
    }

    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Energy{" +
                "id=" + id +
                ", imei='" + imei + '\'' +
                ", pow1='" + pow1 + '\'' +
                ", grouppow='" + grouppow + '\'' +
                ", vol18v1='" + vol18v1 + '\'' +
                ", vol18v2='" + vol18v2 + '\'' +
                ", vol24='" + vol24 + '\'' +
                ", vol28='" + vol28 + '\'' +
                ", extendpow='" + extendpow + '\'' +
                ", solarpow='" + solarpow + '\'' +
                ", operating_temp='" + operating_temp + '\'' +
                ", charge='" + charge + '\'' +
                ", work=" + work +
                ", colltime='" + colltime + '\'' +
                ", deptid='" + deptid + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}