package cn.ljobin.bibi.domain.ms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 串口管理对象 com
 * 
 * @author lyb
 * @date 2020-04-26
 */
@TableName("com")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Com
{

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 串口号 */
    private String com;

    /** 波特率 */
    private String baudrate;

    /** 状态 */
    private String status;

    /** 药房 */
    private Long yaoRoom;

    /** 创建时间 */
    private String controltime;

    /** 注释说明该com口是用于啥的 */
    private String tips;

}
