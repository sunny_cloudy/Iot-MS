package cn.ljobin.bibi.domain.ms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 病人信息对象 patient_info
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("patient_info")
public class PatientInfo
{

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 姓名 */
    private String name;

    /** 性别 */
    private String sex;

    /** 出生日期 */
    private String birthday;

    /** 住址 */
    private String address;

    /** 身高  / 厘米 */
    private Long height;

    /** 体重  /斤 */
    private Long weight;

    /** 对应的医师 */
    private Long did;

    /** 电话号码 */
    private String phone;

}
