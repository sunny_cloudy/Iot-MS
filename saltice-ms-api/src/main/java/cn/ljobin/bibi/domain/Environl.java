package cn.ljobin.bibi.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("tb_environment")
public class Environl {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    private String imei;

    private Float ambient_temp;

    private Float ambient_hum;

    private Float ambient_light;

    private Float soil_temp;

    private Float soil_hum;

    private Float atmo_pressure;

    private Float wind_speed;

    private String wind_direction;

    private String snow_rain;

    private Float pm25;

    private Float co2;

    private Float co;

    private Float so2;

    private String colltime;
    @TableField(exist = false)
    private String endtime;
    @TableField(exist = false)
    private String status;
    @TableField(exist = false)
    private String uid;
    @TableField(exist = false)
    private String username;

    private String deptid;

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    @Override
    public String toString() {
        return "Environl{" +
                "id='" + id + '\'' +
                ", imei='" + imei + '\'' +
                ", ambient_temp='" + ambient_temp + '\'' +
                ", ambient_hum='" + ambient_hum + '\'' +
                ", ambient_light='" + ambient_light + '\'' +
                ", soil_hum='" + soil_hum + '\'' +
                ", atmo_pressure='" + atmo_pressure + '\'' +
                ", wind_speed='" + wind_speed + '\'' +
                ", wind_direction='" + wind_direction + '\'' +
                ", soil_temp='" + soil_temp + '\'' +
                ", snow_rain='" + snow_rain + '\'' +
                ", pm25='" + pm25 + '\'' +
                ", co2='" + co2 + '\'' +
                ", co='" + co + '\'' +
                ", so2='" + so2 + '\'' +
                ", colltime='" + colltime + '\'' +
                ", status='" + status + '\'' +
                ", username='" + username + '\'' +
                ", deptid='" + deptid + '\'' +
                ", uid='" + uid + '\'' +
                '}';
    }


    public Environl() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Float getAmbient_temp() {
        return ambient_temp;
    }

    public void setAmbient_temp(Float ambient_temp) {
        this.ambient_temp = ambient_temp;
    }

    public Float getAmbient_hum() {
        return ambient_hum;
    }

    public void setAmbient_hum(Float ambient_hum) {
        this.ambient_hum = ambient_hum;
    }

    public Float getAmbient_light() {
        return ambient_light;
    }

    public void setAmbient_light(Float ambient_light) {
        this.ambient_light = ambient_light;
    }

    public Float getSoil_temp() {
        return soil_temp;
    }

    public void setSoil_temp(Float soil_temp) {
        this.soil_temp = soil_temp;
    }

    public Float getSoil_hum() {
        return soil_hum;
    }

    public void setSoil_hum(Float soil_hum) {
        this.soil_hum = soil_hum;
    }

    public Float getAtmo_pressure() {
        return atmo_pressure;
    }

    public void setAtmo_pressure(Float atmo_pressure) {
        this.atmo_pressure = atmo_pressure;
    }

    public Float getWind_speed() {
        return wind_speed;
    }

    public void setWind_speed(Float wind_speed) {
        this.wind_speed = wind_speed;
    }

    public String getWind_direction() {
        return wind_direction;
    }

    public void setWind_direction(String wind_direction) {
        this.wind_direction = wind_direction;
    }

    public String getSnow_rain() {
        return snow_rain;
    }

    public void setSnow_rain(String snow_rain) {
        this.snow_rain = snow_rain;
    }

    public Float getPm25() {
        return pm25;
    }

    public void setPm25(Float pm25) {
        this.pm25 = pm25;
    }

    public Float getCo2() {
        return co2;
    }

    public void setCo2(Float co2) {
        this.co2 = co2;
    }

    public Float getCo() {
        return co;
    }

    public void setCo(Float co) {
        this.co = co;
    }

    public Float getSo2() {
        return so2;
    }

    public void setSo2(Float so2) {
        this.so2 = so2;
    }

    public String getColltime() {
        return colltime;
    }

    public void setColltime(String colltime) {
        this.colltime = colltime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeptid() {
        return deptid;
    }

    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }
}