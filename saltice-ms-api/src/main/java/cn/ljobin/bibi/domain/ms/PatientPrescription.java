package cn.ljobin.bibi.domain.ms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 病人药方对象 patient_prescription
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("patient_prescription")
public class PatientPrescription
{

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 病人编号 */
    private Long pid;

    /** 诊断 */
    private String diagnosis;

    /** 药方编号 */
    private String kfid;

    /** 药师编号 */
    private Long did;

    /** 是否取完药 0:未取，1取完 */
    private String tag;

    /** 费用 */
    private String cash;

    /** 药方状态 1:正常 2:草稿 */
    private String type;

    /** 开方时间 */
    private String kfTime;

}
