package cn.ljobin.bibi.model;


import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljobin
 * @since 2019-09-12
 */
public class YaoRoom {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String yaoRoomName;
    private String legalPersonName;
    private String legalPersonPhone;
    private String yaoRoomAddress;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYaoRoomName() {
        return yaoRoomName;
    }

    public void setYaoRoomName(String yaoRoomName) {
        this.yaoRoomName = yaoRoomName;
    }

    public String getLegalPersonName() {
        return legalPersonName;
    }

    public void setLegalPersonName(String legalPersonName) {
        this.legalPersonName = legalPersonName;
    }

    public String getLegalPersonPhone() {
        return legalPersonPhone;
    }

    public void setLegalPersonPhone(String legalPersonPhone) {
        this.legalPersonPhone = legalPersonPhone;
    }

    public String getYaoRoomAddress() {
        return yaoRoomAddress;
    }

    public void setYaoRoomAddress(String yaoRoomAddress) {
        this.yaoRoomAddress = yaoRoomAddress;
    }


    @Override
    public String toString() {
        return "YaoRoom{" +
        "id=" + id +
        ", yaoRoomName=" + yaoRoomName +
        ", legalPersonName=" + legalPersonName +
        ", legalPersonPhone=" + legalPersonPhone +
        ", yaoRoomAddress=" + yaoRoomAddress +
        "}";
    }
}
