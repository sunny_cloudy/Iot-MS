package cn.ljobin.bibi.model;


import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljobin
 * @since 2019-09-12
 */
public class MStream{

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String kfid;
    private String m_name;
    private String dose;
    private String tips;
    private int version;

    public MStream(Integer id, String m_name, String dose, String tips, int version) {
        this.id=id;
        this.m_name = m_name;
        this.dose = dose;
        this.tips = tips;
        this.version = version;
    }
    public MStream( String m_name, String dose, String tips, int version) {
        this.m_name = m_name;
        this.dose = dose;
        this.tips = tips;
        this.version = version;
    }
    public MStream(String kfid, String m_name, String dose, String tips) {
        this.kfid = kfid;
        this.m_name = m_name;
        this.dose = dose;
        this.tips = tips;
    }
    public MStream( String m_name,String dose, String tips) {
        this.m_name = m_name;
        this.dose = dose;
        this.tips = tips;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKfid() {
        return kfid;
    }

    public void setKfid(String kfid) {
        this.kfid = kfid;
    }

    public String getM_name() {
        return m_name;
    }

    public void setM_name(String m_name) {
        this.m_name = m_name;
    }


    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }


    @Override
    public String toString() {
        return "MStream{" +
                "id=" + id +
                ", kfid='" + kfid + '\'' +
                ", m_name='" + m_name + '\'' +
                ", dose='" + dose + '\'' +
                ", tips='" + tips + '\'' +
                ", version=" + version +
                '}';
    }
}
