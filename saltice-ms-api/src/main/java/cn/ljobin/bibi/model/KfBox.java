package cn.ljobin.bibi.model;

/**
 * @program: guns
 * @description: 药店柜子数据
 * @author: Mr.Liu
 * @create: 2019-10-01 00:16
 **/
public class KfBox {
    private int r_x;
    private int r_y;
    private int yaoRoom;
    private String boxName;

    public KfBox(int r_x, int r_y, String boxName) {
        this.r_x = r_x;
        this.r_y = r_y;
        this.boxName = boxName;
    }

    public KfBox(int r_x, int r_y, int yaoRoom, String boxName) {
        this.r_x = r_x;
        this.r_y = r_y;
        this.yaoRoom = yaoRoom;
        this.boxName = boxName;
    }

    @Override
    public String toString() {
        return "KfBox{" +
                "r_x=" + r_x +
                ", r_y=" + r_y +
                ", yaoRoom=" + yaoRoom +
                ", boxName='" + boxName + '\'' +
                '}';
    }

    public int getR_x() {
        return r_x;
    }

    public void setR_x(int r_x) {
        this.r_x = r_x;
    }

    public int getR_y() {
        return r_y;
    }

    public void setR_y(int r_y) {
        this.r_y = r_y;
    }

    public int getYaoRoom() {
        return yaoRoom;
    }

    public void setYaoRoom(int yaoRoom) {
        this.yaoRoom = yaoRoom;
    }

    public String getBoxName() {
        return boxName;
    }

    public void setBoxName(String boxName) {
        this.boxName = boxName;
    }
}
