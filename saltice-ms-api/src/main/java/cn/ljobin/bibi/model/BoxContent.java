package cn.ljobin.bibi.model;

/**
 * @program: guns
 * @description:
 * @author: Mr.Liu
 * @create: 2020-02-24 14:16
 **/
public class BoxContent {
        String boxName;
        String address_x;
        String address_y;
        String yaoName;
        // 1表示要取
        int tag;
        int order;

        public  BoxContent(String boxName, String address_x, String address_y, String yaoName, int tag, int order) {
            this.boxName = boxName;
            this.address_x = address_x;
            this.address_y = address_y;
            this.yaoName = yaoName;
            this.tag = tag;
            this.order = order;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public String getYaoName() {
            return yaoName;
        }

        public int getTag() {
            return tag;
        }

        public void setTag(int tag) {
            this.tag = tag;
        }

        public void setYaoName(String yaoName) {
            this.yaoName = yaoName;
        }

        public BoxContent() {
        }

        public BoxContent(String boxName, String address_x, String address_y,String yaoName,int tag) {
            this.boxName = boxName;
            this.address_x = address_x;
            this.address_y = address_y;
            this.yaoName = yaoName;
            this.tag=tag;
        }
        public BoxContent(String boxName, String address_x, String address_y,String yaoName) {
            this.boxName = boxName;
            this.address_x = address_x;
            this.address_y = address_y;
            this.yaoName = yaoName;
            this.tag=0;
        }

        public String getBoxName() {
            return boxName;
        }

        public void setBoxName(String boxName) {
            this.boxName = boxName;
        }

        public String getAddress_x() {
            return address_x;
        }

        public void setAddress_x(String address_x) {
            this.address_x = address_x;
        }

        public String getAddress_y() {
            return address_y;
        }

        public void setAddress_y(String address_y) {
            this.address_y = address_y;
        }
    }
