package cn.ljobin.bibi.model;

import lombok.Data;

/**
 * @program: guns
 * @description: 串口
 * @author: Mr.Liu
 * @create: 2019-10-07 14:00
 **/
@Data
public class Com {
    /**连接号**/
    private int id;
    /**串口号**/
    private String com;
    /**波特率**/
    private String baudrate;
    /**创建时间**/
    private String controlTime;
    /**com口说明**/
    private String tips;
    /**状态 1：在线 2：停止**/
    private char status;

    public Com(int id, String com, String baudrate, String tips,char status,String controlTime) {
        this.id = id;
        this.com = com;
        this.baudrate = baudrate;
        this.status = status;
        this.controlTime = controlTime;
        this.tips = tips;
    }

    public Com(String com, String baudrate, String controlTime, char status) {
        this.com = com;
        this.baudrate = baudrate;
        this.controlTime = controlTime;
        this.status = status;
    }

    public Com(int id, String com, String baudrate, char status,String controlTime) {
        this.id = id;
        this.com = com;
        this.baudrate = baudrate;
        this.controlTime = controlTime;
        this.status = status;
    }

    public Com(String com, String baudrate, String tips, char status, String controlTime) {
        this.com = com;
        this.baudrate = baudrate;
        this.status = status;
        this.controlTime = controlTime;
        this.tips = tips;
    }

    public Com() {
    }
}
