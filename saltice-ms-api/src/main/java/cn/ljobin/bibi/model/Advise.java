package cn.ljobin.bibi.model;

/**
 * @program: guns
 * @description: 医嘱信息
 * @author: Mr.Liu
 * @create: 2019-11-05 13:29
 **/
public class Advise {
    int id;
    String kfid;
    int way;
    String num;
    String eat_time;
    String taboo;
    String advise;

    @Override
    public String toString() {
        return "Advise{" +
                "id=" + id +
                ", kfid='" + kfid + '\'' +
                ", way=" + way +
                ", num='" + num + '\'' +
                ", eat_time='" + eat_time + '\'' +
                ", taboo='" + taboo + '\'' +
                ", advise='" + advise + '\'' +
                '}';
    }

    public Advise(int id, String kfid, int way, String num, String eat_time, String taboo, String advise) {
        this.id = id;
        this.kfid = kfid;
        this.way = way;
        this.num = num;
        this.eat_time = eat_time;
        this.taboo = taboo;
        this.advise = advise;
    }

    public Advise(int way, String num, String eat_time, String taboo, String advise) {
        this.way = way;
        this.num = num;
        this.eat_time = eat_time;
        this.taboo = taboo;
        this.advise = advise;
    }

    public Advise(String kfid, int way, String num, String eat_time, String taboo, String advise) {
        this.kfid = kfid;
        this.way = way;
        this.num = num;
        this.eat_time = eat_time;
        this.taboo = taboo;
        this.advise = advise;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKfid() {
        return kfid;
    }

    public void setKfid(String kfid) {
        this.kfid = kfid;
    }

    public int getWay() {
        return way;
    }

    public void setWay(int way) {
        this.way = way;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getEat_time() {
        return eat_time;
    }

    public void setEat_time(String eat_time) {
        this.eat_time = eat_time;
    }

    public String getTaboo() {
        return taboo;
    }

    public void setTaboo(String taboo) {
        this.taboo = taboo;
    }

    public String getAdvise() {
        return advise;
    }

    public void setAdvise(String advise) {
        this.advise = advise;
    }
}
