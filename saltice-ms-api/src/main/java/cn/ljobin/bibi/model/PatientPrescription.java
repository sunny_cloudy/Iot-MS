package cn.ljobin.bibi.model;

import lombok.Data;
import lombok.ToString;

/**
 * @program: guns
 * @description: 病人药方数据
 * @author: Mr.Liu
 * @create: 2019-11-10 19:34
 **/
@Data
@ToString
public class PatientPrescription {
        int id;
        /**病人编号**/
        Long pid;
        String diagnosis;
        String kfid;
        /**医师编号**/
        Long did;
        /**是否取完药 0:未取，1取完**/
        String tag;
        String cash;
        /**药方状态 1:正常 2:草稿**/
        String type;
        /**医师姓名**/
        String dName;
    String kf_time;

    public String getKf_time() {
        return kf_time;
    }

    public void setKf_time(String kf_time) {
        this.kf_time = kf_time;
    }

    public PatientPrescription(Long pid, String kf_time, String diagnosis, Long did, String tag, String cash, String type) {
        this.pid = pid;
        this.diagnosis = diagnosis;
        this.did = did;
        this.tag = tag;
        this.cash = cash;
        this.type = type;
        this.kf_time = kf_time;
    }

    public PatientPrescription(Long pid, String kf_time, String diagnosis, String kfid, Long did,
                               String tag, String cash, String type) {
        this.pid = pid;
        this.diagnosis = diagnosis;
        this.kfid = kfid;
        this.did = did;
        this.tag = tag;
        this.cash = cash;
        this.type = type;
        this.kf_time = kf_time;
    }

    public PatientPrescription(String diagnosis, String kfid, Long did, String tag, String cash, String type, String kf_time) {
        this.diagnosis = diagnosis;
        this.kfid = kfid;
        this.did = did;
        this.tag = tag;
        this.cash = cash;
        this.type = type;
        this.kf_time = kf_time;
    }

    public PatientPrescription(int id, Long pid, String diagnosis, String kfid,
                               Long did, String tag, String cash, String type, String kf_time) {
        this.id = id;
        this.pid = pid;
        this.diagnosis = diagnosis;
        this.kfid = kfid;
        this.did = did;
        this.tag = tag;
        this.cash = cash;
        this.type = type;
        this.kf_time = kf_time;
    }

}
