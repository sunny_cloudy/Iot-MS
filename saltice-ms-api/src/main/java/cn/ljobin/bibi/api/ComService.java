package cn.ljobin.bibi.api;

import cn.ljobin.bibi.model.Com;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: guns
 * @description: 串口
 * @author: Mr.Liu
 * @create: 2019-10-07 14:11
 **/
@RequestMapping("/Feign/ComService")
@RestController
public interface ComService {
    /**
     * 获取药房所有串口配置数据
     * @param yao_room
     * @return
     */
    @GetMapping("/selectAllCom")
    public List<Com> selectAllCom(@RequestParam("yao_room") int yao_room);
    /**
     * 新增串口配置数据
     * @param com
     * @return
     */
    @PostMapping("/insertCom")
    public int insertCom(@RequestParam("com") Com com);
    /**
     * 更新串口配置数据
     * @param com
     * @return
     */
    @PostMapping("/updateCom")
    public int updateCom(@RequestParam("com") Com com, @RequestParam("yao_room") int yao_room);
    /**
     * 删除串口配置数据
     * @param com
     * @return
     */
    @PostMapping("/deleteCom")
    public int deleteCom(@RequestParam("com") Com com, @RequestParam("yao_room") int yao_room);
}
