package cn.ljobin.bibi.api;

import cn.ljobin.bibi.domain.Torrent;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @program: IoT-Plat
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-11 15:26
 **/
@RequestMapping("/iot/data")
public interface IotDataApi {
    /**
     * 获取终端信息
     * @return
     */
    @RequestMapping(value = "/torrent", method = RequestMethod.POST)
    public List<Torrent> getTorrentList(@RequestBody Torrent torrent);
}
