package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.ms.Com;
import java.util.List;

/**
 * 串口管理Service接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface IComService 
{
    /**
     * 查询串口管理
     * 
     * @param id 串口管理ID
     * @return 串口管理
     */
    public Com selectComById(Long id);

    /**
     * 查询串口管理列表
     * 
     * @param com 串口管理
     * @return 串口管理集合
     */
    public List<Com> selectComList(Com com);

    /**
     * 新增串口管理
     * 
     * @param com 串口管理
     * @return 结果
     */
    public int insertCom(Com com);

    /**
     * 修改串口管理
     * 
     * @param com 串口管理
     * @return 结果
     */
    public int updateCom(Com com);

    /**
     * 批量删除串口管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteComByIds(String ids);

    /**
     * 删除串口管理信息
     * 
     * @param id 串口管理ID
     * @return 结果
     */
    public int deleteComById(Long id);
}
