package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.AdviseMapper;
import cn.ljobin.bibi.domain.ms.Advise;
import cn.ljobin.bibi.service.IAdviseService;

/**
 * 医嘱信息Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class AdviseServiceImpl implements IAdviseService 
{
    @Autowired
    private AdviseMapper adviseMapper;

    /**
     * 查询医嘱信息
     * 
     * @param id 医嘱信息ID
     * @return 医嘱信息
     */
    @Override
    public Advise selectAdviseById(Long id)
    {
        return adviseMapper.selectAdviseById(id);
    }

    /**
     * 查询医嘱信息列表
     * 
     * @param advise 医嘱信息
     * @return 医嘱信息
     */
    @Override
    public List<Advise> selectAdviseList(Advise advise)
    {
        return adviseMapper.selectAdviseList(advise);
    }

    /**
     * 新增医嘱信息
     * 
     * @param advise 医嘱信息
     * @return 结果
     */
    @Override
    public int insertAdvise(Advise advise)
    {
        return adviseMapper.insertAdvise(advise);
    }

    /**
     * 修改医嘱信息
     * 
     * @param advise 医嘱信息
     * @return 结果
     */
    @Override
    public int updateAdvise(Advise advise)
    {
        return adviseMapper.updateAdvise(advise);
    }

    /**
     * 删除医嘱信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteAdviseByIds(String ids)
    {
        return adviseMapper.deleteAdviseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除医嘱信息信息
     * 
     * @param id 医嘱信息ID
     * @return 结果
     */
    @Override
    public int deleteAdviseById(Long id)
    {
        return adviseMapper.deleteAdviseById(id);
    }
}
