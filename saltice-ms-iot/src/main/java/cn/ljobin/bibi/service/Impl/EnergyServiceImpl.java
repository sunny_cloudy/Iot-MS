package cn.ljobin.bibi.service.Impl;
import cn.ljobin.bibi.domain.Energy;
import cn.ljobin.bibi.mapper.EnergyMapper;
import cn.ljobin.bibi.service.IEnergyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @program:  EnergySys
 * @description: 能源管理系统
 * @author: lyb
 * @create: 2019-12-25 14:20
 **/
@Service
public class EnergyServiceImpl extends ServiceImpl<EnergyMapper, Energy> implements IEnergyService {

    @Override
    public boolean insert(Energy energy) {
        return this.save(energy);
    }
}
