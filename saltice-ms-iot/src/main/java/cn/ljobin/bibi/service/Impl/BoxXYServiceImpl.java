package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.BoxXYMapper;
import cn.ljobin.bibi.domain.ms.BoxXY;
import cn.ljobin.bibi.service.IBoxXYService;

/**
 * 每个药柜的大小Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class BoxXYServiceImpl implements IBoxXYService 
{
    @Autowired
    private BoxXYMapper boxXYMapper;

    /**
     * 查询每个药柜的大小
     * 
     * @param id 每个药柜的大小ID
     * @return 每个药柜的大小
     */
    @Override
    public BoxXY selectBoxXYById(Long id)
    {
        return boxXYMapper.selectBoxXYById(id);
    }

    /**
     * 查询每个药柜的大小列表
     * 
     * @param boxXY 每个药柜的大小
     * @return 每个药柜的大小
     */
    @Override
    public List<BoxXY> selectBoxXYList(BoxXY boxXY)
    {
        return boxXYMapper.selectBoxXYList(boxXY);
    }

    /**
     * 新增每个药柜的大小
     * 
     * @param boxXY 每个药柜的大小
     * @return 结果
     */
    @Override
    public int insertBoxXY(BoxXY boxXY)
    {
        return boxXYMapper.insertBoxXY(boxXY);
    }

    /**
     * 修改每个药柜的大小
     * 
     * @param boxXY 每个药柜的大小
     * @return 结果
     */
    @Override
    public int updateBoxXY(BoxXY boxXY)
    {
        return boxXYMapper.updateBoxXY(boxXY);
    }

    /**
     * 删除每个药柜的大小对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBoxXYByIds(String ids)
    {
        return boxXYMapper.deleteBoxXYByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除每个药柜的大小信息
     * 
     * @param id 每个药柜的大小ID
     * @return 结果
     */
    @Override
    public int deleteBoxXYById(Long id)
    {
        return boxXYMapper.deleteBoxXYById(id);
    }
}
