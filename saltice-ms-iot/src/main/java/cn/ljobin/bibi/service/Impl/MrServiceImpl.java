package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.MrMapper;
import cn.ljobin.bibi.domain.ms.Mr;
import cn.ljobin.bibi.service.IMrService;

/**
 * 取药开始的地方，由用户自行设置Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class MrServiceImpl implements IMrService 
{
    @Autowired
    private MrMapper mrMapper;

    /**
     * 查询取药开始的地方，由用户自行设置
     * 
     * @param id 取药开始的地方，由用户自行设置ID
     * @return 取药开始的地方，由用户自行设置
     */
    @Override
    public Mr selectMrById(Long id)
    {
        return mrMapper.selectMrById(id);
    }

    /**
     * 查询取药开始的地方，由用户自行设置列表
     * 
     * @param mr 取药开始的地方，由用户自行设置
     * @return 取药开始的地方，由用户自行设置
     */
    @Override
    public List<Mr> selectMrList(Mr mr)
    {
        return mrMapper.selectMrList(mr);
    }

    /**
     * 新增取药开始的地方，由用户自行设置
     * 
     * @param mr 取药开始的地方，由用户自行设置
     * @return 结果
     */
    @Override
    public int insertMr(Mr mr)
    {
        return mrMapper.insertMr(mr);
    }

    /**
     * 修改取药开始的地方，由用户自行设置
     * 
     * @param mr 取药开始的地方，由用户自行设置
     * @return 结果
     */
    @Override
    public int updateMr(Mr mr)
    {
        return mrMapper.updateMr(mr);
    }

    /**
     * 删除取药开始的地方，由用户自行设置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMrByIds(String ids)
    {
        return mrMapper.deleteMrByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除取药开始的地方，由用户自行设置信息
     * 
     * @param id 取药开始的地方，由用户自行设置ID
     * @return 结果
     */
    @Override
    public int deleteMrById(Long id)
    {
        return mrMapper.deleteMrById(id);
    }
}
