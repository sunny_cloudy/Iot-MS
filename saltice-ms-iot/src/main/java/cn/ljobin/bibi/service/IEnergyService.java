package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.Energy;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @program: EnergySys
 * @description: 能源管理系统
 * @author:
 * @create: 2019-12-25 14:20
 **/
public interface IEnergyService extends IService<Energy> {
    /**
     * 插入
     * @param energy
     * @return
     */
    public boolean insert(Energy energy);
}
