package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.YaoRoomKfMapper;
import cn.ljobin.bibi.domain.ms.YaoRoomKf;
import cn.ljobin.bibi.service.IYaoRoomKfService;


/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class YaoRoomKfServiceImpl implements IYaoRoomKfService 
{
    @Autowired
    private YaoRoomKfMapper yaoRoomKfMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public YaoRoomKf selectYaoRoomKfById(Long id)
    {
        return yaoRoomKfMapper.selectYaoRoomKfById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param yaoRoomKf 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<YaoRoomKf> selectYaoRoomKfList(YaoRoomKf yaoRoomKf)
    {
        return yaoRoomKfMapper.selectYaoRoomKfList(yaoRoomKf);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param yaoRoomKf 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertYaoRoomKf(YaoRoomKf yaoRoomKf)
    {
        return yaoRoomKfMapper.insertYaoRoomKf(yaoRoomKf);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param yaoRoomKf 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateYaoRoomKf(YaoRoomKf yaoRoomKf)
    {
        return yaoRoomKfMapper.updateYaoRoomKf(yaoRoomKf);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteYaoRoomKfByIds(String ids)
    {
        return yaoRoomKfMapper.deleteYaoRoomKfByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteYaoRoomKfById(Long id)
    {
        return yaoRoomKfMapper.deleteYaoRoomKfById(id);
    }
}
