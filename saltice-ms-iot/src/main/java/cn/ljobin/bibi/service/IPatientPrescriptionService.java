package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.ms.PatientPrescription;
import java.util.List;

/**
 * 病人药方Service接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface IPatientPrescriptionService 
{
    /**
     * 查询病人药方
     * 
     * @param id 病人药方ID
     * @return 病人药方
     */
    public PatientPrescription selectPatientPrescriptionById(Long id);

    /**
     * 查询病人药方列表
     * 
     * @param patientPrescription 病人药方
     * @return 病人药方集合
     */
    public List<PatientPrescription> selectPatientPrescriptionList(PatientPrescription patientPrescription);

    /**
     * 新增病人药方
     * 
     * @param patientPrescription 病人药方
     * @return 结果
     */
    public int insertPatientPrescription(PatientPrescription patientPrescription);

    /**
     * 修改病人药方
     * 
     * @param patientPrescription 病人药方
     * @return 结果
     */
    public int updatePatientPrescription(PatientPrescription patientPrescription);

    /**
     * 批量删除病人药方
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePatientPrescriptionByIds(String ids);

    /**
     * 删除病人药方信息
     * 
     * @param id 病人药方ID
     * @return 结果
     */
    public int deletePatientPrescriptionById(Long id);
}
