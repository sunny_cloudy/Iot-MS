package cn.ljobin.bibi.service;


import cn.ljobin.bibi.domain.Environl;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


public interface IEnvironlService extends IService<Environl> {
    /**
     * 插入
     * @param environl
     * @return
     */
    public boolean insert(Environl environl);
}