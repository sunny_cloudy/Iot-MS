package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.ms.YaoRoom;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface IYaoRoomService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public YaoRoom selectYaoRoomById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param yaoRoom 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<YaoRoom> selectYaoRoomList(YaoRoom yaoRoom);

    /**
     * 新增【请填写功能名称】
     * 
     * @param yaoRoom 【请填写功能名称】
     * @return 结果
     */
    public int insertYaoRoom(YaoRoom yaoRoom);

    /**
     * 修改【请填写功能名称】
     * 
     * @param yaoRoom 【请填写功能名称】
     * @return 结果
     */
    public int updateYaoRoom(YaoRoom yaoRoom);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteYaoRoomByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteYaoRoomById(Long id);
}
