package cn.ljobin.bibi.service;


import cn.ljobin.bibi.domain.Torrent;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 终端运转 服务层
 * 
 * @author 刘衍斌
 * @date 2019-03-03
 */
public interface ITorrentService extends IService<Torrent> {
    /**
     * 终端设备上下线
     * @param torrent
     * @return
     */
    public int onlineOrDisOnline(Torrent torrent);

    /**
     * 获取列表
     * @param torrent
     * @return
     */
    public List<Torrent> selectTorrentList(Torrent torrent);
}
