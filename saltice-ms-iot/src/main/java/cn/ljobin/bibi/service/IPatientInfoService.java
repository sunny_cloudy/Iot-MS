package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.ms.PatientInfo;
import java.util.List;

/**
 * 病人信息Service接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface IPatientInfoService 
{
    /**
     * 查询病人信息
     * 
     * @param id 病人信息ID
     * @return 病人信息
     */
    public PatientInfo selectPatientInfoById(Long id);

    /**
     * 查询病人信息列表
     * 
     * @param patientInfo 病人信息
     * @return 病人信息集合
     */
    public List<PatientInfo> selectPatientInfoList(PatientInfo patientInfo);

    /**
     * 新增病人信息
     * 
     * @param patientInfo 病人信息
     * @return 结果
     */
    public int insertPatientInfo(PatientInfo patientInfo);

    /**
     * 修改病人信息
     * 
     * @param patientInfo 病人信息
     * @return 结果
     */
    public int updatePatientInfo(PatientInfo patientInfo);

    /**
     * 批量删除病人信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePatientInfoByIds(String ids);

    /**
     * 删除病人信息信息
     * 
     * @param id 病人信息ID
     * @return 结果
     */
    public int deletePatientInfoById(Long id);
}
