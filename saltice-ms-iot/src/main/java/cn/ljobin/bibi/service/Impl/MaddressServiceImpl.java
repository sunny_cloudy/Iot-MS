package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.MaddressMapper;
import cn.ljobin.bibi.domain.ms.Maddress;
import cn.ljobin.bibi.service.IMaddressService;


/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class MaddressServiceImpl implements IMaddressService 
{
    @Autowired
    private MaddressMapper maddressMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public Maddress selectMaddressById(Long id)
    {
        return maddressMapper.selectMaddressById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param maddress 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Maddress> selectMaddressList(Maddress maddress)
    {
        return maddressMapper.selectMaddressList(maddress);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param maddress 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMaddress(Maddress maddress)
    {
        return maddressMapper.insertMaddress(maddress);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param maddress 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMaddress(Maddress maddress)
    {
        return maddressMapper.updateMaddress(maddress);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMaddressByIds(String ids)
    {
        return maddressMapper.deleteMaddressByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMaddressById(Long id)
    {
        return maddressMapper.deleteMaddressById(id);
    }
}
