package cn.ljobin.bibi.service.Impl;

import cn.ljobin.bibi.api.ComService;
import cn.ljobin.bibi.model.Com;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.ComMapper;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 串口管理Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class ComServiceImpl implements ComService {
    @Autowired
    private ComMapper comMapper;
    @Override
    public List<Com> selectAllCom(@RequestParam("yao_room") int yao_room) {
        return comMapper.selectAllCom(yao_room);
    }

    @Override
    public int insertCom(Com com) {
        return comMapper.insertCom(com);
    }

    @Override
    public int updateCom(Com com, int yao_room) {
        return comMapper.updateCom(com,yao_room);
    }

    @Override
    public int deleteCom(Com com, int yao_room) {
        return comMapper.deleteCom(com,yao_room);
    }

}
