package cn.ljobin.bibi.mapper;

import cn.ljobin.bibi.domain.ms.YaoRoomKf;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface YaoRoomKfMapper extends BaseMapper<YaoRoomKf>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public YaoRoomKf selectYaoRoomKfById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param yaoRoomKf 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<YaoRoomKf> selectYaoRoomKfList(YaoRoomKf yaoRoomKf);

    /**
     * 新增【请填写功能名称】
     * 
     * @param yaoRoomKf 【请填写功能名称】
     * @return 结果
     */
    public int insertYaoRoomKf(YaoRoomKf yaoRoomKf);

    /**
     * 修改【请填写功能名称】
     * 
     * @param yaoRoomKf 【请填写功能名称】
     * @return 结果
     */
    public int updateYaoRoomKf(YaoRoomKf yaoRoomKf);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteYaoRoomKfById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteYaoRoomKfByIds(String[] ids);
}
