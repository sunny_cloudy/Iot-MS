package cn.ljobin.bibi.mapper;

import cn.ljobin.bibi.model.Com;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 串口管理Mapper接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface ComMapper
{
    /**
     * 获取药房所有串口配置数据
     * @param yao_room
     * @return
     */
    public List<Com> selectAllCom(@Param("yao_room") int yao_room);
    /**
     * 新增串口配置数据
     * @param com
     * @return
     */
    public int insertCom(@Param("com") Com com);
    /**
     * 更新串口配置数据
     * @param com
     * @return
     */
    public int updateCom(@Param("com") Com com,@Param("yao_room") int yao_room);
    /**
     * 删除串口配置数据
     * @param com
     * @return
     */
    public int deleteCom(@Param("com") Com com,@Param("yao_room") int yao_room);
}
