package cn.ljobin.bibi.mapper;

import cn.ljobin.bibi.domain.ms.YaoRoom;
import cn.ljobin.bibi.domain.ms.YaoRoomDoctor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface YaoRoomDoctorMapper extends BaseMapper<YaoRoomDoctor>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public YaoRoomDoctor selectYaoRoomDoctorById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param yaoRoomDoctor 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<YaoRoomDoctor> selectYaoRoomDoctorList(YaoRoomDoctor yaoRoomDoctor);

    /**
     * 新增【请填写功能名称】
     * 
     * @param yaoRoomDoctor 【请填写功能名称】
     * @return 结果
     */
    public int insertYaoRoomDoctor(YaoRoomDoctor yaoRoomDoctor);

    /**
     * 修改【请填写功能名称】
     * 
     * @param yaoRoomDoctor 【请填写功能名称】
     * @return 结果
     */
    public int updateYaoRoomDoctor(YaoRoomDoctor yaoRoomDoctor);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteYaoRoomDoctorById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteYaoRoomDoctorByIds(String[] ids);
}
