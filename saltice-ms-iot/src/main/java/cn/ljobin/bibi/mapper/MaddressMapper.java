package cn.ljobin.bibi.mapper;

import cn.ljobin.bibi.domain.ms.Maddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface MaddressMapper extends BaseMapper<Maddress>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public Maddress selectMaddressById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param maddress 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Maddress> selectMaddressList(Maddress maddress);

    /**
     * 新增【请填写功能名称】
     * 
     * @param maddress 【请填写功能名称】
     * @return 结果
     */
    public int insertMaddress(Maddress maddress);

    /**
     * 修改【请填写功能名称】
     * 
     * @param maddress 【请填写功能名称】
     * @return 结果
     */
    public int updateMaddress(Maddress maddress);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMaddressById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMaddressByIds(String[] ids);
}
