package cn.ljobin.bibi.mapper;


import cn.ljobin.bibi.domain.Environl;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
@DS("master")
public interface EnvironlMapper extends BaseMapper<Environl> {

}