package cn.ljobin.bibi.roses.nettyserver;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;

/**
 * @author liuyanbin
 */
public class HeartbeatServerHandler extends SimpleChannelInboundHandler {

    public HeartbeatServerHandler(boolean autoRelease) {
        super(autoRelease);
    }

    /**
     * Return a unreleasable view on the given ByteBuf
     * which will just ignore release and retain calls.
     * 定义的心跳包类型
     * **/
    private static final ByteBuf HEARTBEAT_SEQUENCE = Unpooled
            .unreleasableBuffer(Unpooled.copiedBuffer("Heartbeat : Please send again",
                    CharsetUtil.UTF_8));
    private static final ByteBuf HEARTBEAT_CLOSE = Unpooled
            .unreleasableBuffer(Unpooled.copiedBuffer("Closed : Please reconnect",
                    CharsetUtil.UTF_8));
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
            throws Exception {

        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            String type = "";
            if (event.state() == IdleState.READER_IDLE) {
                type = "read idle";
            } else if (event.state() == IdleState.WRITER_IDLE) {
                type = "write idle";
            } else if (event.state() == IdleState.ALL_IDLE) {
                type = "all idle";
            }
            System.out.println("超时类型："+type);
            final ByteBuf buf = ctx.alloc().buffer().writeBytes("ping".getBytes());
            ctx.writeAndFlush(buf);
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {

    }
}
