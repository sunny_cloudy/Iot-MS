package cn.ljobin.bibi.roses.common.persist;

import cn.ljobin.bibi.common.RedisUtil;
import cn.ljobin.bibi.domain.Energy;
import cn.ljobin.bibi.domain.Environl;
import cn.ljobin.bibi.domain.Torrent;
import cn.ljobin.bibi.roses.utils.SpringContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: ruoyi
 * @description: redis
 * @author: Mr.Liu
 * @create: 2020-03-24 11:22
 **/

public class RedisMemery implements BaseMo{
    private RedisUtil redisUtil=(RedisUtil) SpringContextUtils.getBeanByClass(RedisUtil.class);
    @Override
    public boolean insert(Object msg) {
        if(redisUtil==null){
            return false;
        }
        if(msg instanceof Environl){
            try{
                String s = redisUtil.toJson(msg);
                redisUtil.listRightSet("iot_environment",s);
                return true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if(msg instanceof Energy){
            try{
                String s = redisUtil.toJson(msg);
                redisUtil.listRightSet("iot_energy",s);
                return true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean update(Object msg) {
        if(redisUtil==null){
            return false;
        }
        if(msg instanceof Torrent){
            try {
                String s = redisUtil.toJson(msg);
                redisUtil.listRightSet("iot_ioterminal",s);
                return true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return false;
    }
}
