package cn.ljobin.bibi.roses.common.persist;

import cn.ljobin.bibi.domain.Energy;
import cn.ljobin.bibi.domain.Environl;
import cn.ljobin.bibi.domain.Torrent;
import cn.ljobin.bibi.roses.utils.SpringContextUtils;
import cn.ljobin.bibi.service.IEnergyService;
import cn.ljobin.bibi.service.IEnvironlService;
import cn.ljobin.bibi.service.ITorrentService;
import cn.ljobin.bibi.service.Impl.EnergyServiceImpl;
import cn.ljobin.bibi.service.Impl.EnvironlServiceImpl;
import cn.ljobin.bibi.service.Impl.TorrentServiceImpl;
import cn.ljobin.bibi.utils.AnnotationUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: ruoyi
 * @description: mysql
 * @author: Mr.Liu
 * @create: 2020-03-24 11:13
 **/

public class Memery implements BaseMo{
    private static IEnvironlService tbEnvironmentService = (EnvironlServiceImpl) SpringContextUtils.getBeanByClass(EnvironlServiceImpl.class);
    private static IEnergyService tbEnergyService = (EnergyServiceImpl) SpringContextUtils.getBeanByClass(EnergyServiceImpl.class);
    private static ITorrentService tbIoterminalService = (TorrentServiceImpl) SpringContextUtils.getBeanByClass(TorrentServiceImpl.class);
    private static AnnotationUtils<Torrent> aut = new AnnotationUtils<>();
    @Override
    public boolean insert(Object msg) {
        if(msg instanceof Environl){
            return tbEnvironmentService.save((Environl)msg);
        }
        if(msg instanceof Energy){
            return tbEnergyService.save((Energy)msg);
        }
        return false;
    }

    @Override
    public boolean update(Object msg) {
        if(msg instanceof Torrent){
            QueryWrapper<Torrent> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("imei",((Torrent) msg).getImei());
            return tbIoterminalService.update((Torrent) msg,queryWrapper);
        }
        return false;
    }
}
