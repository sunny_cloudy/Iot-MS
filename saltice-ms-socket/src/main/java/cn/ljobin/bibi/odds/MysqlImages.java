package cn.ljobin.bibi.odds;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author liuyanbin
 */
public class MysqlImages {
    /**定义数据库连接参数**/
    public static final String DRIVER_CLASS_NAME = "com.mysql.cj.jdbc.Driver";
    public static final String URL2 = "jdbc:mysql://47.103.1.210:3306/iot_agps?autoReconnect=true&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&serverTimezone=CTT ";
    public static final String URL = "jdbc:mysql://47.103.1.210:3306/iot_Netty?autoReconnect=true&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&serverTimezone=CTT ";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "root";

    // 注册数据库驱动
    static {
        try {
            Class.forName(DRIVER_CLASS_NAME);
            /**System.out.println("注册数据库驱动成功");**/
        } catch (ClassNotFoundException e) {
            System.out.println("注册失败！"+e.getMessage());
        }
    }

    // 获取连接
    public static Connection getConn() throws SQLException {
        /**System.out.println("获取数据库连接成功！");**/
        return DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }
    // 获取连接2
    public static Connection getConn2() throws SQLException {
        /**System.out.println("获取数据库连接成功！");**/
        return DriverManager.getConnection(URL2, USERNAME, PASSWORD);
    }
    // 关闭连接
    public static void closeConn(Connection conn) {
        if (null != conn) {
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println("关闭连接失败！"+e.getMessage());
            }
        }
    }

//    // 测试
//    public static void main(String[] args) throws SQLException {
//        System.out.println(mysqlimages.getConn());
//        closeConn(mysqlimages.getConn());
//    }
}
