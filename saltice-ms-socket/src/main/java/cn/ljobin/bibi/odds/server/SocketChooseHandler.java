package cn.ljobin.bibi.odds.server;

import cn.ljobin.bibi.odds.unity.SpringContextByClassUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-11 11:44
 **/
//@ChannelHandler.Sharable 这个是标识该handler是共享的

/**
 * 加上这个@ChannelHandler.Sharable注解，当前Handler就是一个共享的Handler，不用每个channel都new 一个实例，
 * 方便统计一些全局信息。
 * 被注解的ChannelHandler对应的同一个实例可以被加入到一个或者多个ChannelPipelines一次或者多次，而不会存在竞争条件。
 * sharable注解定义在ChannelHandler接口里面，该注解被使用是在ChannelHandlerAdapter类里面，
 * 被sharable注解标记过的实例都会存入当前加载线程的threadlocal里面，
 *
 * 要求是被Sharable注解的共享channelHandler才能被添加至不同的channel中；
 * 另外业务线程也有可能并发调用IO方法如read,write等等，这样会出现线程安全问题吗？
 * 答案：执行IO任务时会转回为IO线程，也就是NioEventLoop，他会将IO方法分装成一个task,串行执行。因此此类方法不会产生并发问题。
 *
 * 比如所有连接报错次数（exceptionCaught）、统计流量之类等等
 * 注意：
 *      使用这种线程共享的handler可以避免频繁创建handler带来的系统开销
 *      适用于某些支持线程共享的handler，比如日志服务，计数服务等。
 *      适用于没有成员变量的encoder、decoder
 **/
public class SocketChooseHandler extends ByteToMessageDecoder {
    private static Logger logger = LoggerFactory.getLogger(SocketChooseHandler.class);
    /** 默认暗号长度为23 */
    private static final int MAX_LENGTH = 23;
    /** WebSocket握手的协议前缀 */
    private static final String WEBSOCKET_PREFIX = "GET /";
    private static final String WEBSOCKET_PREFIX_POST = "POST /";
    /**盆栽数据标志**/
    public static final String POT_TAG = "55";
    /**花式注入redis服务，不能用@Autowired注入**/
    private static PipelineAdd redisUtil;
    static {
        redisUtil = SpringContextByClassUtil.getBean(PipelineAdd.class);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        String protocol = getBufStart(in);
        //logger.info(protocol);
        if (protocol.startsWith(WEBSOCKET_PREFIX) || protocol.startsWith(WEBSOCKET_PREFIX_POST)) {
            redisUtil.websocketAdd(ctx);
            //对于 webSocket ，不设置超时断开
        }else{
            if(devDecoder(in)){
                redisUtil.devAdd(ctx);
            }else if(serverDecoder(in)){
                redisUtil.serverAdd(ctx);
            }else if(potDevDecoder(in)){
                redisUtil.potDevAdd(ctx);
            }else {
                logger.error("【警告】有人异常连接");
                ctx.close();
            }
        }
        in.resetReaderIndex();
        ctx.pipeline().remove(this.getClass());
    }

    /**
     * 药柜设备检测
     * @param in
     * @return
     */
    public boolean devDecoder(ByteBuf in){
        String s1 = "1010";
        String s2 = "55";
        String s3 = "61";
        String s4 = "40";
        int d = in.readableBytes();
        if (d < 7) {
            //logger.error("包字节数不符合");
            return false;
        }
        in.markReaderIndex();
        byte[] datas = new byte[d];
        in.readBytes(datas);
        in.resetReaderIndex();
        if(!s1.equals(BinaryToHexString(datas[0])) && !s2.equals(BinaryToHexString(datas[1]))){
            //logger.error("不是硬件数据包头");
            return false;
        }
        if(!s3.equals(BinaryToHexString(datas[2]))){
            //logger.error("不是硬件数据");
            return false;
        }
        //包头信息，两字节 0xAA 0x55  不考虑了
//        if(!s2.equals(BinaryToHexString(datas[d-2])) && !s4.equals(BinaryToHexString(datas[d-1]))){
//            logger.error("包尾错误");
//            return false;
//        }
        return true;
    }
    /**
     * 盆栽设备检测
     * @param in
     * @return
     */
    public boolean potDevDecoder(ByteBuf in){
        //0x0A
        String s1 = "0010";
        //英文逗号 ,  0x2C
        String s3 = "0212";
        int d = in.readableBytes();
        //数据长度小于7直接断开
        if (d < 7) {
            return false;
        }
        in.markReaderIndex();
        byte[] datas = new byte[d];
        in.readBytes(datas);
        in.resetReaderIndex();
        if(!s1.equals(BinaryToHexString(datas[0])) && !POT_TAG.equals(BinaryToHexString(datas[1]))
                && !s3.equals(BinaryToHexString(datas[2]))){
            logger.error("不是盆栽硬件数据包头");
            return false;
        }
//        if(!s2.equals(BinaryToHexString(datas[d-2])) && !s4.equals(BinaryToHexString(datas[d-1]))){
//            logger.error("包尾错误");
//            return false;
//        }
        return true;
    }
    /**
     * 服务器检测
     * @param in
     * @return
     */
    public boolean serverDecoder(ByteBuf in){
        String s1 = "53";
        String s2 = "55";
        String s3 = "66";
        int d = in.readableBytes();
        if (d < 4) {
            //logger.error("包字节数不符合");
            return false;
        }
        in.markReaderIndex();
        byte[] datas = new byte[d];
        in.readBytes(datas);
        in.resetReaderIndex();
        if(!s1.equals(BinaryToHexString(datas[0])) && !s2.equals(BinaryToHexString(datas[1]))){
            //logger.error("不是服务器数据包头");
            return false;
        }
        if(!s3.equals(BinaryToHexString(datas[2]))){
            //logger.error("不是服务器数据");
            return false;
        }
        //包头信息，两字节 0xAA 0x55  不考虑了
//        if(!s2.equals(BinaryToHexString(datas[d-2])) && !s4.equals(BinaryToHexString(datas[d-1]))){
//            logger.error("包尾错误");
//            return false;
//        }
        return true;
    }

    /**
     * 返回的String是String类型
     * @param bytes
     * @return
     */
    public static String BinaryToHexString(byte bytes){
        String hex;
        //字节高4位
        hex = String.valueOf((bytes&0xF0)>>4);
        //字节低4位
        hex += String.valueOf(bytes&0x0F);
        return hex;
    }

    /**
     * 返回的String是StringBuilder转来的String
     * @param bytes
     * @return
     */
    public static String BinaryToHexString(byte[] bytes){

        StringBuilder result = new StringBuilder();
        String hex;
        int b = bytes.length;
        for (int i=0;i<b;i++){
            //字节高4位
            hex = String.valueOf((bytes[i]&0xF0)>>4);
            //字节低4位
            hex += String.valueOf(bytes[i]&0x0F);
            result.append(hex).append(",");
        }
        return result.toString();
    }

    /**
     * 将字节数组从i1 到 i2转为String ，该String是String类型
     * @param bytes
     * @param i1 开始的位置
     * @param i2 结束的位置
     * @return
     */
    public static String BinaryToHexString(byte[] bytes,int i1 , int i2){

        if(i1 < 0 && i2 < 0){
            throw  new NumberFormatException("参数不能小于0");
        }
        if(i1 > i2 || i2>bytes.length){
            throw new NumberFormatException("参数不合法");
        }
        byte[] newByte = new byte[i2-i1];
        for (int i=i1,k=0;i<i2;i++,k++){
            newByte[k] = bytes[i];
        }
        return new String(newByte);
    }

    private String getBufStart(ByteBuf in){
        int length = in.readableBytes();
        if (length > MAX_LENGTH) {
            length = MAX_LENGTH;
        }

        // 标记读位置
        in.markReaderIndex();
        byte[] content = new byte[length];
        in.readBytes(content);
        in.resetReaderIndex();
        return new String(content);
    }
}
