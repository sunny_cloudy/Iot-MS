package cn.ljobin.bibi.odds.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/**
 * @program: Netty
 * @description:
 * @author: Mr.Liu
 * @create: 2019-04-11 17:07
 **/
public class HeartBeatServerChannelHandler extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {

        /**
         * 当服务器接受到完成TCP三次握手链接的时候给当前完成握手的Channel通道创建一个ChannelPipeline，
         * 并且创建一个SocketChooseHandler的实例加入到Channel通道的ChannelPipeline，
         * 再在SocketChooseHandler里面进行连接类型判断，再进行最终的分配Handler到ChannelPipeline
         * 但是SocketChooseHandler加上了@Sharable 注解，就是一个共享复用的Handler
         **/

        /**
         * 最初的连接类型判断 websocket、socket（硬件、系统后台）
         **/
        ch.pipeline().addLast("socket",new SocketChooseHandler());
        /**
         * 下面的在判断是哪种类型后再动态添加处理逻辑
         */
//        //读\写超时状态处理
//        ch.pipeline().addLast("idleStateHandler", new IdleStateHandler(0,
//                0, Const.HEARTCHECKINTERVAL, TimeUnit.SECONDS));
//        ByteBuf delimiter= Unpooled.copiedBuffer("@".getBytes());
//        //创建DelimiterBasedFrameDecoder对象，将其加入到ChannelPipeline
//        //参数1024表示单条消息的最大长度，当达到该长度仍然没有找到分隔符就抛出TooLongFrame异常，第二个参数就是分隔符
//        //由于DelimiterBasedFrameDecoder自动对请求消息进行了解码，下面的ChannelHandler接收到的msg对象就是完整的消息包
//        ch.pipeline().addLast("serverDecoder",new DelimiterBasedFrameDecoder(1024, delimiter));
//        //ch.pipeline().addLast(new FixedLengthFrameDecoder(10));
//        //StringDecoder解码器将ByteBuf解码成字符串对象，这样在ChannelHandlerAdapter中读取消息时就不需要通过ByteBuf获取了
//        ch.pipeline().addLast(new StringDecoder());
//        //设置连接数
//        ch.pipeline().addLast(new ConnectHandler(new AtomicInteger(2)));
//        ch.pipeline().addLast(new HeartbeatServerHandler());

    }
}
