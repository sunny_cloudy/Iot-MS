package cn.ljobin.bibi.odds.server.decoder;

import cn.ljobin.bibi.odds.server.SocketChooseHandler;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-05 15:37
 **/
public class DevDecoder extends MessageToMessageDecoder<ByteBuf> {

        private int index = 0;

        public DevDecoder(int index) {
            this.index = index;
        }
         public DevDecoder() {
        }
        @Override
        public void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
            //可以选择去掉头部两个字节，这里没有去掉了
            int ten = msg.readableBytes()-index;
            byte[] array = new byte[ten];
            //index 是msg中数据开始的位置，从index开始
            // array数组是用来接收的数组
            // 0 为array数组中从 0 开始的多少个位置的数据不要了，这里为0 ，就是没有丢弃掉数据
            // ten 是array数组中 ten 位置后面的数据不要了，这里为ten ，就是没有丢弃掉数据
           msg.getBytes(index, array,0,ten);
            String head2 = "55";
            String head1 = "1010";
            String end2 = "1010";
            String end1 = "55";
            if(ten > 4
                    && SocketChooseHandler.BinaryToHexString(array[0]).equals(head1)
                    && SocketChooseHandler.BinaryToHexString(array[1]).equals(head2)
                    && SocketChooseHandler.BinaryToHexString(array[ten-2]).equals(end1)
                    && SocketChooseHandler.BinaryToHexString(array[ten-1]).equals(end2)){
                out.add(array);
            }else {
                //格式不对直接断开连接 ， 可以考虑接收几次，要是还错误再断开
                System.err.println("有人乱来，断开连接");
                ctx.close();
            }

        }
    }
