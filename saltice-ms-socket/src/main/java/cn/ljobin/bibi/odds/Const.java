package cn.ljobin.bibi.odds;

/**
 * @program: AGPS
 * @description: 全局变量
 * @author: Mr.Liu
 * @create: 2019-03-09 17:24
 **/
public class Const {
    /**数据存入数据库是否正确的返回码**/
    public static final String SECCESSF = "Seccess";
    public static final String ERROR = "Error";
    /**异常退出**/
    public static final String EXCEPTION = "Abnormal exit";
    /**设备未注册**/
    public static final String  UNREGISTERED = "Unregistered";
    /**错误*/
    public static final String BAD_ORDER = "BAD ORDER";
    /**数据格式错误**/
    public static final String BAD_FORMAT = "Data Format Error";

    /**运行 状态码**/
    public static final String RUN_STATUS = "1";
    /**停止 状态码**/
    public static final String STOP_STATUS = "0";
    /**限制心跳包数**/
    public static final int HEARD_NUM = 2;
    /**60秒检测一次设备状态**/
    public static final int HEARTCHECKINTERVAL = 60;
    /**
     * 设备返回常量
     */
    /** 成功的返回码**/
    public static final String S_INT= "200";
    /**错误的返回码**/
    public static final String E_INT = "501";
    /**数据格式错误**/
    public static final String F_ERROR = "401";
    /**没有目标设备**/
    public static final String NO_DEV = "400";
}
