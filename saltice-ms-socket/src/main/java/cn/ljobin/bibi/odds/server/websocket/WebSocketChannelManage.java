package cn.ljobin.bibi.odds.server.websocket;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * WebSocketChannel的管理
 * @author liuyazhuang
 *
 */
public class WebSocketChannelManage {
    public static String webSocket = "webSocket";
    //微信头
    public static String wxSocket = "wx";
	/**
	 * 存储每一个WebSocket客户端接入进来时的channel对象
	 */
	public static ChannelGroup webSocketGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /**存储连接中的WebSocketChannel <服务器编号 对应的Channel/>**/
    public static ConcurrentMap<String, Channel> webSocketIdAndChannelMap = new ConcurrentHashMap<>();

    /**用来存储用户账号和websocket ID 的绑定**/
    public static ConcurrentMap<String,String> userIdAndSocketId = new ConcurrentHashMap<>();


    public static boolean hasChannel(String id) {
        Channel channel = webSocketIdAndChannelMap.get(id);
        if (channel == null) {
            return false;
        } else {
            return true;
        }
    }

}
