package cn.ljobin.bibi.odds.server.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-12 12:57
 **/
public class ServerStringDecoder extends MessageToMessageDecoder<ByteBuf>{


    private int index = 0;

    public ServerStringDecoder(int index) {
        this.index = index;
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        //去掉头部两个字节
        byte[] array = new byte[msg.readableBytes()-index];
        msg.getBytes(index, array);
        out.add(new String(array, StandardCharsets.UTF_8));
    }
    }
