package cn.ljobin.bibi.odds.unity;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-04 20:04
 **/

@Component
public class SpringContextByNameUtil implements ApplicationContextAware {
    /**
     * 获取spring容器，以访问容器中定义的其他bean
     */
    private static ApplicationContext contexts;
    private static DefaultListableBeanFactory defaultListableBeanFactory;
    /**
     * 实现ApplicationContextAware接口的回调方法，设置上下文环境
     *
     * @param context
     */
    @Override
    public void setApplicationContext(ApplicationContext context)
            throws BeansException {
        contexts = context;
        defaultListableBeanFactory = (DefaultListableBeanFactory)context.getAutowireCapableBeanFactory();
    }

    /**
     * @return ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
        return contexts;
    }
    /**
     * 获取对象
     * 这里重写了bean方法，起主要作用
     * @param beanName
     * @return Object 一个以所给名字注册的bean的实例
     * @throws BeansException
     */

    public static<T> T getBean(String beanName) throws BeansException{
        return (T) contexts.getBean(beanName);
    }
    public static void destroy() {
        contexts= null;
    }
    public static String getMessage(String key) {
        return contexts.getMessage(key, null, Locale.getDefault());
    }
}
