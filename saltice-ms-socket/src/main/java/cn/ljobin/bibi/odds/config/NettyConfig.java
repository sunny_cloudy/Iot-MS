package cn.ljobin.bibi.odds.config;

import cn.ljobin.bibi.odds.FixedSizeThreadPool;
import cn.ljobin.bibi.roses.nettyserver.HeartBeatServer;

import javax.annotation.PostConstruct;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-02 17:58
 **/
//@Component
public class NettyConfig {
    @PostConstruct
    public void netty() throws Exception {
        FixedSizeThreadPool pool = new FixedSizeThreadPool(1,1);
        pool.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("一个线程被放入到我们的仓库中。。。");
                HeartBeatServer.func();
            }
        });
    }

}
