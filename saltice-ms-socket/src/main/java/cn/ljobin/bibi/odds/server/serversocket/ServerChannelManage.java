package cn.ljobin.bibi.odds.server.serversocket;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 服务器设备的管理
 * @author liuyazhuang
 *
 */
public class ServerChannelManage {

	/**
	 * 存储每一个服务器客户端接入进来时的channel对象
	 */
	public static ChannelGroup serverGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /**存储连接中的服务器 <服务器编号 对应的Channel/>**/
    public static ConcurrentMap<String, Channel> serverIdAndChannelMap = new ConcurrentHashMap<>();



    public static boolean hasChannel(String id) {
        Channel channel = serverIdAndChannelMap.get(id);
        if (channel == null) {
            return false;
        } else {
            return true;
        }
    }

}
