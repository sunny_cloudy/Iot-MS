//package cn.ljobin.bibi.config.activemq;
//import cn.ljobin.bibi.enums.MessageQueueEnum;
//import org.apache.activemq.ActiveMQConnectionFactory;
//import org.apache.activemq.command.ActiveMQQueue;
//import org.apache.activemq.command.ActiveMQTopic;
//import org.springframework.context.annotation.Bean;
//import org.springframework.jms.connection.SingleConnectionFactory;
//import org.springframework.jms.core.JmsTemplate;
//
///**
// * @program: Iot-MS
// * @description:
// * @author: Mr.Liu
// * @create: 2020-06-06 14:15
// **/
//public class ActiveMqConfig {
//    @Bean
//    //配置ConnectionFactory用于生成connection
//    public ActiveMQConnectionFactory connectionFactory() {
//        ActiveMQConnectionFactory activeMQConnectionFactory
//                = new ActiveMQConnectionFactory("tcp://localhost:61616");
//        return activeMQConnectionFactory;
//    }
//    @Bean
//    //注册SingleConnectionFactory,这个spring的一个包装工厂 用于管理真正的ConnectionFactory
//    public SingleConnectionFactory singleConnectionFactory(ActiveMQConnectionFactory activeMQconnectionFactory) {
//        SingleConnectionFactory connectionFactory = new SingleConnectionFactory();
//        //设置目标工厂
//        connectionFactory.setTargetConnectionFactory(activeMQconnectionFactory);
//        return connectionFactory;
//    }
//    @Bean
//    //配置生产者，jmsTemplate
//    public JmsTemplate jmsTemplate(SingleConnectionFactory connectionFactory) {
//        JmsTemplate jmsTemplate = new JmsTemplate();
//        jmsTemplate.setConnectionFactory(connectionFactory);
//        return jmsTemplate;
//    }
//    /**
//     * 配置队列目的的： 根据测试需要配置其中一个
//     * 1.队列  点对点 queue
//     * 2.主题  一对多  topic
//     */
//    @Bean
//    public ActiveMQQueue queueDestination() {
//        ActiveMQQueue activeMQQueue = new ActiveMQQueue(MessageQueueEnum.MAKE_MONITOR_MESSAGE.name());
//        return activeMQQueue;
//    }
//
////    @Bean
////    public ActiveMQTopic topicDestination() {
////        ActiveMQTopic activeMQTopic = new ActiveMQTopic("topic-anno");
////        return activeMQTopic;
////    }
//}
