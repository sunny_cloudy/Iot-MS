package cn.ljobin.bibi.config.netty;

import cn.ljobin.bibi.netty.handler.RxtxHandler;
import cn.ljobin.bibi.utils.ThreadPool.FixedSizeThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

/**
 * @program: iot
 * @description: netty配置类
 * @author: Mr.Liu
 * @create: 2019-05-09 17:17
 **/

@Configuration
public class NettyConfig {

    public static String S_URL = "47.103.1.210";
    public static String PORT = "8090";
    /**测试账号**/
    public static final String TEST_UID = "client";
    private FixedSizeThreadPool pool = new FixedSizeThreadPool(2,2);
    /**运行COM口数据处理任务**/
    public static final FixedSizeThreadPool comPool = new FixedSizeThreadPool(2,2);
    private final static Logger logger = LoggerFactory.getLogger(NettyConfig.class);
    public static String D_ID = RxtxHandler.DID;
//    @Bean
//    public String netty() {
//        try {
//            logger.info("netty客户端启动成功,ip:{},port:{}",S_URL,PORT);
//            pool.submit(new Runnable() {
//                @Override
//                public void run() {
//                    System.out.println("netty接收客户端线程被放入到我们的仓库中。。。"+Thread.currentThread().getName());
//                    try {
//                        NettyClient.run(S_URL,Integer.parseInt(PORT));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//            logger.info("netty运行异常：");
//        }
//        return "ok";
//    }
    //@Bean
//    public String nettyCom() {
//        try {
//            logger.info("Comm启动成功,comm口:COM14,波特率:115200");
//            comPool.submit(new Runnable() {
//                @Override
//                public void run() {
//                    System.out.println("netty接收客户端线程被放入到我们的仓库中。。。"+Thread.currentThread().getName());
//                    try {
//                        NettyCom nettyCom = new NettyCom();
//                        nettyCom.StarNewCom("COM14",115200,NettyConfig.TEST_UID);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//            logger.info("Comm运行异常：");
//        }
//        return "ok";
//    }

}
