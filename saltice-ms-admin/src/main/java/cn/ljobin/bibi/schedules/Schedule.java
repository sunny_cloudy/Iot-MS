package cn.ljobin.bibi.schedules;

import cn.ljobin.bibi.api.IkfService;
import cn.ljobin.bibi.model.KfBox;
import cn.ljobin.bibi.model.MStream;
import cn.ljobin.bibi.utils.ThreadPool.FixedSizeThreadPool;
import cn.ljobin.bibi.utils.cache.Cache;
import cn.ljobin.bibi.utils.pathplan.MapNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;

import static cn.ljobin.bibi.utils.pathplan.ant.Main.antGetPath;


/**
 * @program: MAYUN
 * @description: 定时器任务
 * @author: Mr.Liu
 * @create: 2019-01-20 13:43
 **/
@Component
public class Schedule {

    @Autowired
    IkfService ikfService;

    private static final Logger logger = LoggerFactory.getLogger(Schedule.class);
    private static final SimpleDateFormat DATA_TIME = new SimpleDateFormat("HH:mm:ss");

    /**实例化一个线程池，用来处理计算路径的任务，每次可以同时运行 5 个，任务池大小为10**/
    private static final FixedSizeThreadPool POOL = new FixedSizeThreadPool(5,10);

    /**
     * 正常处理队列
     */
    static ConcurrentLinkedDeque<KfTask> kfTaskConcurrentLinkedDequeCom = new ConcurrentLinkedDeque<KfTask>();
    static {
        //kfTaskConcurrentLinkedDequeCom.add(new KfTask("3254a04580f042c59d170a1e95952605",0));
        //kfTaskConcurrentLinkedDequeCom.add(new KfTask("9704e964959c4d7fa984c6f197d7160b",0));
        //kfTaskConcurrentLinkedDequeCom.add(new KfTask("84b51d22fe9b4934bd50be2b11e7a167",0));
        //kfTaskConcurrentLinkedDequeCom.add(new KfTask("8c42012f84c948c89e7bcc8a83628589",0));
        //kfTaskConcurrentLinkedDequeCom.add(new KfTask("f02835c8ea8e457ebd38dbe58a88b457",0));
    }
    /**
     * 紧急处理队列
     */
    static ConcurrentLinkedDeque<KfTask> kfTaskConcurrentLinkedDequeNow = new ConcurrentLinkedDeque<KfTask>();

    public static void addQueTask(String kfid){
        if (kfid==null|| "".equals(kfid))
            throw new NullPointerException();
        KfTask kfTask = new KfTask(kfid,0);
        if(kfTask.getTag()==1)
            kfTaskConcurrentLinkedDequeNow.add(kfTask);
        if (kfTask.getTag()==0)
            kfTaskConcurrentLinkedDequeCom.add(kfTask);
    }
    /**
     * 更新 药方取药路径规划  正常
     * 一分钟一次
     */
    @Scheduled(fixedRate = 1000*60)
    public void updatePathCom(){
        logger.info("更新 药方取药路径规划  正常");
        if(!kfTaskConcurrentLinkedDequeCom.isEmpty()){
            while (!kfTaskConcurrentLinkedDequeCom.isEmpty()){
                KfTask kfTask = kfTaskConcurrentLinkedDequeCom.pop();
                POOL.submit(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("一个线程被放入到我们的仓库中。。。");
                        List<MStream> kfs = ikfService.getMedicine(kfTask.getKfId());
                        //现阶段只有一个药房 1 号
                        String ontPoint = ikfService.selectYaoRoomBeginPath(1);
                        List<KfBox> kfBoxList = ikfService.selectKfBox(1);

                        String boxNameOne = ontPoint.split("-->")[0];
                        int offset = getOffset(kfBoxList,boxNameOne);
                        int[] x_y_1 = get_X_Y(ontPoint.split("-->")[1],offset);
                        //用来记录不重复的数据
                        List<MapNode> cityNodeList = new ArrayList<>();
                        //起始点，原始位置置为 -1， 如果药方单中有这个的话，会在其num中后面添加对应的index
                        cityNodeList.add(new MapNode(x_y_1[0],x_y_1[1],-1));
                        //对取药顺序进行 优化 计算出最优路径 ,用原字段 id 充当排序号
                        for (int i = 0; i < kfs.size(); i++) {
                            MStream v = kfs.get(i);
                            String boxName = v.getTips().split("-->")[0];
                            int offset2 = getOffset(kfBoxList,boxName);
                            int[] x_y = get_X_Y(v.getTips().split("-->")[1],offset2);
                            //创建一个对象，保存x 和 y ，再添加该值在kfs中的位置哦
                            MapNode mapNode = new MapNode(x_y[0],x_y[1],i);
                            //MapNode的对象比较是只看x 和 y 的值是否一致
                            if(!cityNodeList.contains(mapNode)){
                                //把不包含在cityNodeList的对象放进去cityNodeList
                                cityNodeList.add(mapNode);
                            }else {
                                //存在cityNodeList中的对象则更新其中保存值在 kfs 中的位置
                                // 在Array<Integer>保存
                                int index = cityNodeList.indexOf(mapNode);
                                cityNodeList.get(index).andIndex(i);
                            }
                        }
                        //通过蚂蚁算法计算最优路径，返回对应的序号
                        int[] path = antGetPath(cityNodeList);
                        int pathLength = path.length;
                        int[] results = new int[kfs.size()+1];
                        AtomicInteger index = new AtomicInteger(0);
                        for (int i = 0; i < pathLength ; i++) {
                            cityNodeList.get(path[i]).getNum().forEach(v->{
                                //剔除起始点，但是不会剔除与起始点相同的点
                                if(v != -1){
                                    results[index.getAndIncrement()] = v;
                                }
                            });
                        }
                        int[] pathResults = new int[kfs.size()+1];
                        for (int i = 0; i < kfs.size(); i++) {
                            for (int j = 0 ;j<results.length-1;j++){
                                if(results[j]==i){
                                    pathResults[i] = j;
                                    break;
                                }
                            }
                        }
                        //计算后放入缓存
                        if(Cache.get("kfPath"+kfTask.getKfId())==null) {
                            Cache.put("kfPath" + kfTask.getKfId(), pathResults, 60 * 60 * 24 * 30, true);
                        }else {
                            Cache.remove("kfPath"+kfTask.getKfId());
                            Cache.put("kfPath" + kfTask.getKfId(), pathResults, 60 * 60 * 24 * 30, true);
                        }
                    }
                });

            }
        }
    }

    /**
     * 求偏移量的
     * @return
     */
    public static int getOffset(List<KfBox> kfBoxList,String boxNameOne){
        AtomicInteger atomicInteger = new AtomicInteger(0);
        AtomicInteger xx = new AtomicInteger(0);
        kfBoxList.forEach(v->{
            if(boxNameOne.equals(v.getBoxName())){
                atomicInteger.getAndIncrement();
            }else {
                if (atomicInteger.get()==0){
                    xx.addAndGet(v.getR_x());
                }
            }
        });
        //药柜偏移量
        return xx.get();
    }
    /**
     * 返回 【x数值 y数值】
     * @param v
     * @param offset  y的偏移量
     * @return
     */
    public static int[] get_X_Y(String v,int offset){
        int[] results = new int[2];
        String x = v.split("，")[0];
        int x_1 = Integer.parseInt(x.substring(1,x.length()-1));
        results[0] = x_1;
        String y = v.split("，")[1];
        int y_1 = Integer.parseInt(y.substring(1,y.length()-1))+offset;
        results[1] = y_1;
        return results;
    }
    /**
     * 更新 药方取药路径规划  紧急
     * 半小时一次 向数据库查询一次未进行路径规划的开方单id
     */
    @Scheduled(fixedRate = 1000*60*30)
    public void updatePathNow(){
        logger.warn("正在添加");
        ikfService.selectAllKfNoPath2().forEach(v->{
            if(Cache.get("kfPath" + v)==null){
                kfTaskConcurrentLinkedDequeCom.add(new KfTask(v,0));
            }
        });

        //kfTaskConcurrentLinkedDequeCom.add(new KfTask("f02835c8ea8e457ebd38dbe58a88b457",0));
    }
    /**
     * 更新 活着的COM口处理线程状态
     * 半分钟一次
     */
    @Scheduled(fixedRate = 1000*30)
    public void updateCom(){

    }
//    @Scheduled(fixedRate = 2000*60*30)
//    public void likes(){
//        logger.info("<<你好>>：{}",123);
//    }
}
